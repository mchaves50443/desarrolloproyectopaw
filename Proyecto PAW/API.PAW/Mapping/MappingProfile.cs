﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;

namespace API.PAW.Mapping
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<data.TablaPrograma, Models.TablaPrograma>().ReverseMap();
            CreateMap<data.TablaEjercicios, Models.TablaEjercicios>().ReverseMap();
            CreateMap<data.TablaProgramaEjercicio, Models.TablaProgramaEjercicio>().ReverseMap();
            CreateMap<data.TablaActividades, Models.TablaActividades>().ReverseMap(); 
            CreateMap<data.TablaCategoria, Models.TablaCategoria>().ReverseMap();
            CreateMap<data.TablaUsuario, Models.TablaUsuario>().ReverseMap();
            CreateMap<data.TablaProveedor, Models.TablaProveedor>().ReverseMap();
            CreateMap<data.TablaProducto, Models.TablaProducto>().ReverseMap();
            CreateMap<data.TablaPedido, Models.TablaPedido>().ReverseMap();
           
            CreateMap<data.TablaLog, Models.TablaLog>().ReverseMap();
            CreateMap<data.TablaCompra, Models.TablaCompra>().ReverseMap();
            CreateMap<data.TablaRutina, Models.TablaRutina>().ReverseMap();
        }
    }
}
