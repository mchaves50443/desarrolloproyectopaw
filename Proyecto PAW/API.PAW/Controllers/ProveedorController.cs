﻿using AutoMapper;
using data = BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models = API.PAW.Models;

namespace API.PAW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProveedorController : ControllerBase
    {
        private readonly DBContext dbcontext;
        private readonly IMapper mapper;
        public ProveedorController(DBContext _dbcontext, IMapper _mapper)
        {
            dbcontext = _dbcontext;
            mapper = _mapper;
        }

        // GET: api/TablaProveedor
        [HttpGet]
        public async Task<ActionResult<IEnumerable<models.TablaProveedor>>> GetTablaProveedor()
        {
            var res = new BE.BS.Proveedor(dbcontext).GetAll();
            var mapaux = mapper.Map<IEnumerable<data.TablaProveedor>, IEnumerable<models.TablaProveedor>>(res).ToList();
            return mapaux;
        }

        // GET: api/TablaProveedor/5
        [HttpGet("{id}")]
        public async Task<ActionResult<models.TablaProveedor>> GetTablaProveedor(long id)
        {
            var TablaProveedor = new BE.BS.Proveedor(dbcontext).GetOneById(id);

            if (TablaProveedor == null)
            {
                return NotFound();
            }
            var mapaux = mapper.Map<data.TablaProveedor, models.TablaProveedor>(TablaProveedor);
            return mapaux;
        }

        // PUT: api/TablaProveedor/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTablaProveedor(long id, models.TablaProveedor TablaProveedor)
        {
            if (id != TablaProveedor.IdProveedor)
            {
                return BadRequest();
            }

            try
            {
                var mapaux = mapper.Map<models.TablaProveedor, data.TablaProveedor>(TablaProveedor);
                new BE.BS.Proveedor(dbcontext).Update(mapaux);
            }
            catch (Exception ee)
            {
                if (!TablaProveedorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TablaProveedor
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<models.TablaProveedor>> PostTablaProveedor(models.TablaProveedor TablaProveedor)
        {
            var mapaux = mapper.Map<models.TablaProveedor, data.TablaProveedor>(TablaProveedor);
            new BE.BS.Proveedor(dbcontext).Insert(mapaux);

            return CreatedAtAction("GetTablaProveedor", new { id = TablaProveedor.IdProveedor }, TablaProveedor);
        }

        // DELETE: api/TablaProveedor/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<models.TablaProveedor>> DeleteTablaProveedor(int id)
        {
            var TablaProveedor = new BE.BS.Proveedor(dbcontext).GetOneById(id);
            if (TablaProveedor == null)
            {
                return NotFound();
            }

            new BE.BS.Proveedor(dbcontext).Delete(TablaProveedor);
            var mapaux = mapper.Map<data.TablaProveedor, models.TablaProveedor>(TablaProveedor);
            return mapaux;
        }

        private bool TablaProveedorExists(long id)
        {
            return (new BE.BS.Proveedor(dbcontext).GetOneById(id) != null);
        }
    }
}

