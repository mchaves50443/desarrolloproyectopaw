﻿using AutoMapper;
using data = BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models = API.PAW.Models;

namespace API.PAW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActividadesController : ControllerBase
    {
        private readonly DBContext dbcontext;
        private readonly IMapper mapper;
        public ActividadesController(DBContext _dbcontext, IMapper _mapper)
        {
            dbcontext = _dbcontext;
            mapper = _mapper;
        }

        // GET: api/TablaActividades
        [HttpGet]
        public async Task<ActionResult<IEnumerable<models.TablaActividades>>> GetTablaActividades()
        {
            var res = new BE.BS.Actividades(dbcontext).GetAll();
            var mapaux = mapper.Map<IEnumerable<data.TablaActividades>, IEnumerable<models.TablaActividades>>(res).ToList();
            return mapaux;
        }

        // GET: api/TablaActividades/5
        [HttpGet("{id}")]
        public async Task<ActionResult<models.TablaActividades>> GetTablaActividades(long id)
        {
            var TablaActividades = new BE.BS.Actividades(dbcontext).GetOneById(id);

            if (TablaActividades == null)
            {
                return NotFound();
            }
            var mapaux = mapper.Map<data.TablaActividades, models.TablaActividades>(TablaActividades);
            return mapaux;
        }

        // PUT: api/TablaActividades/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTablaActividades(long id, models.TablaActividades TablaActividades)
        {
            if (id != TablaActividades.IdActividad)
            {
                return BadRequest();
            }

            try
            {
                var mapaux = mapper.Map<models.TablaActividades, data.TablaActividades>(TablaActividades);
                new BE.BS.Actividades(dbcontext).Update(mapaux);
            }
            catch (Exception ee)
            {
                if (!TablaActividadesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TablaActividades
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<models.TablaActividades>> PostTablaActividades(models.TablaActividades TablaActividades)
        {
            var mapaux = mapper.Map<models.TablaActividades, data.TablaActividades>(TablaActividades);
            new BE.BS.Actividades(dbcontext).Insert(mapaux);

            return CreatedAtAction("GetTablaActividades", new { id = TablaActividades.IdActividad }, TablaActividades);
        }

        // DELETE: api/TablaActividades/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<models.TablaActividades>> DeleteTablaActividades(int id)
        {
            var TablaActividades = new BE.BS.Actividades(dbcontext).GetOneById(id);
            if (TablaActividades == null)
            {
                return NotFound();
            }

            new BE.BS.Actividades(dbcontext).Delete(TablaActividades);
            var mapaux = mapper.Map<data.TablaActividades, models.TablaActividades>(TablaActividades);
            return mapaux;
        }

        private bool TablaActividadesExists(long id)
        {
            return (new BE.BS.Actividades(dbcontext).GetOneById(id) != null);
        }
    }
}
