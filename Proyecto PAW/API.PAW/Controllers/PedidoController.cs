﻿using AutoMapper;
using data = BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models = API.PAW.Models;

namespace API.PAW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidoController : ControllerBase
    {
        private readonly DBContext dbcontext;
        private readonly IMapper mapper;
        public PedidoController(DBContext _dbcontext, IMapper _mapper)
        {
            dbcontext = _dbcontext;
            mapper = _mapper;
        }

        // GET: api/Pedido
        [HttpGet]
        public async Task<ActionResult<IEnumerable<models.TablaPedido>>> GetPedido()
        {
            var res = await new BE.BS.Pedido(dbcontext).GetAllAsync();
            var mapaux = mapper.Map<IEnumerable<data.TablaPedido>, IEnumerable<models.TablaPedido>>(res).ToList();

            return mapaux;

            // Este GetAll no trae las relaaciones
            //return new BE.BS.Pedido(dbcontext).GetAll().ToList();
        }

        // GET: api/Pedido/5
        [HttpGet("{id}")]
        public async Task<ActionResult<models.TablaPedido>> GetPedido(int id)
        {
            var Pedido = await new BE.BS.Pedido(dbcontext).GetOneByIdAsync(id);
            var mapaux = mapper.Map<data.TablaPedido, models.TablaPedido>(Pedido);

            // Este Get no trae las relaaciones
            //var Pedido = new BE.BS.Pedido(dbcontext).GetOneById(id);

            if (Pedido == null)
            {
                return NotFound();
            }

            return mapaux;
        }

        // PUT: api/Pedido/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPedido(int id, models.TablaPedido Pedido)
        {
            if (id != Pedido.IdPedido)
            {
                return BadRequest();
            }

            try
            {
                var mapaux = mapper.Map<models.TablaPedido, data.TablaPedido>(Pedido);
                new BE.BS.Pedido(dbcontext).Update(mapaux);
            }
            catch (Exception ee)
            {
                if (!PedidoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Pedido
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<models.TablaPedido>> PostPedido(models.TablaPedido Pedido)
        {
            var mapaux = mapper.Map<models.TablaPedido, data.TablaPedido>(Pedido);
            new BE.BS.Pedido(dbcontext).Insert(mapaux);

            return CreatedAtAction("GetPedido", new { id = Pedido.IdPedido }, Pedido);
        }

        // DELETE: api/Pedido/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<models.TablaPedido>> DeletePedido(int id)
        {
            var Pedido = new BE.BS.Pedido(dbcontext).GetOneById(id);
            if (Pedido == null)
            {
                return NotFound();
            }

            new BE.BS.Pedido(dbcontext).Delete(Pedido);
            var mapaux = mapper.Map<data.TablaPedido, models.TablaPedido>(Pedido);
            return mapaux;
        }

        private bool PedidoExists(int id)
        {
            return (new BE.BS.Pedido(dbcontext).GetOneById(id) != null);
            //return _context.Pedido.Any(e => e.FocusId == id);
        }
    }
}

