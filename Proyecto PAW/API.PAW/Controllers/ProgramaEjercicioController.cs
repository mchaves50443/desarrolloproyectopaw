﻿using AutoMapper;
using data = BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models = API.PAW.Models;
using Microsoft.EntityFrameworkCore;

namespace API.PAW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProgramaEjercicioController : ControllerBase
    {
        private readonly DBContext dbcontext;
        private readonly IMapper mapper;
        public ProgramaEjercicioController(DBContext _dbcontext, IMapper _mapper)
        {
            dbcontext = _dbcontext;
            mapper = _mapper;
        }

        // GET: api/TablaProgramaEjercicio
        [HttpGet]
        public async Task<ActionResult<IEnumerable<models.TablaProgramaEjercicio>>> GetTablaProgramaEjercicio()
        {
            var res = await new BE.BS.ProgramaEjercicio(dbcontext).GetAllAsync();
            var mapaux = mapper.Map<IEnumerable<data.TablaProgramaEjercicio>, IEnumerable<models.TablaProgramaEjercicio>>(res).ToList();

            return mapaux;

            // Este GetAll no trae las relaaciones
            //return new BE.BS.TablaProgramaEjercicio(dbcontext).GetAll().ToList();
        }

        // GET: api/TablaProgramaEjercicio/5
        [HttpGet("{ProgramaEjercicioID}")]
        public async Task<ActionResult<models.TablaProgramaEjercicio>> GetTablaProgramaEjercicio(long ProgramaEjercicioID)
        {
            var TablaPrograma = await new BE.BS.ProgramaEjercicio(dbcontext).GetOneByIdAsync(ProgramaEjercicioID);

            if (TablaPrograma == null)
            {
                return NotFound();
            }
            var mapaux = mapper.Map<data.TablaProgramaEjercicio, models.TablaProgramaEjercicio>(TablaPrograma);
            return mapaux;
        }

        // PUT: api/TablaProgramaEjercicio/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{ProgramaEjercicioID}")]
        public async Task<IActionResult> PutTablaProgramaEjercicio(long ProgramaEjercicioID, models.TablaProgramaEjercicio TablaProgramaEjercicio)
        {
            if (ProgramaEjercicioID != TablaProgramaEjercicio.IdProgramaEjercicio)
            {
                return BadRequest();
            }

            try
            {
                var mapaux = mapper.Map<models.TablaProgramaEjercicio, data.TablaProgramaEjercicio>(TablaProgramaEjercicio);
                new BE.BS.ProgramaEjercicio(dbcontext).Update(mapaux);
            }
            catch (Exception ee)
            {
                if (!TablaProgramaEjercicioExists(ProgramaEjercicioID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TablaProgramaEjercicio
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<models.TablaProgramaEjercicio>> PostTablaProgramaEjercicio(models.TablaProgramaEjercicio TablaProgramaEjercicio)
        {
            var mapaux = mapper.Map<models.TablaProgramaEjercicio, data.TablaProgramaEjercicio>(TablaProgramaEjercicio);
            new BE.BS.ProgramaEjercicio(dbcontext).Insert(mapaux);

            return CreatedAtAction("GetTablaProgramaEjercicio", new { id = TablaProgramaEjercicio.FkIdPrograma }, TablaProgramaEjercicio);
        }

        // DELETE: api/TablaProgramaEjercicio/5
        [HttpDelete("{ProgramaEjercicioID}")]
        public async Task<ActionResult<models.TablaProgramaEjercicio>> DeleteTablaProgramaEjercicio(long ProgramaEjercicioID)
        {
            var TablaProgramaEjercicio = await new BE.BS.ProgramaEjercicio(dbcontext).GetOneByIdAsync(ProgramaEjercicioID);
            if (TablaProgramaEjercicio == null)
            {
                return NotFound();
            }

            new BE.BS.ProgramaEjercicio(dbcontext).Delete(TablaProgramaEjercicio);
            var mapaux = mapper.Map<data.TablaProgramaEjercicio, models.TablaProgramaEjercicio>(TablaProgramaEjercicio);
            return mapaux;
        }

        private bool TablaProgramaEjercicioExists(long id)
        {
            return (new BE.BS.ProgramaEjercicio(dbcontext).GetOneByIdAsync(id) != null);
            
        }
    }
}
