﻿using AutoMapper;
using data = BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models = API.PAW.Models;

namespace API.PAW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        private readonly DBContext dbcontext;
        private readonly IMapper mapper;
        public ProductoController(DBContext _dbcontext, IMapper _mapper)
        {
            dbcontext = _dbcontext;
            mapper = _mapper;
        }

        // GET: api/Producto
        [HttpGet]
        public async Task<ActionResult<IEnumerable<models.TablaProducto>>> GetProducto()
        {
            var res = await new BE.BS.Producto(dbcontext).GetAllAsync();
            var mapaux = mapper.Map<IEnumerable<data.TablaProducto>, IEnumerable<models.TablaProducto>>(res).ToList();

            return mapaux;

            // Este GetAll no trae las relaaciones
            //return new BE.BS.Producto(dbcontext).GetAll().ToList();
        }

        // GET: api/Producto/5
        [HttpGet("{id}")]
        public async Task<ActionResult<models.TablaProducto>> GetProducto(int id)
        {
            var Producto = await new BE.BS.Producto(dbcontext).GetOneByIdAsync(id);
            var mapaux = mapper.Map<data.TablaProducto, models.TablaProducto>(Producto);

            // Este Get no trae las relaaciones
            //var Producto = new BE.BS.Producto(dbcontext).GetOneById(id);

            if (Producto == null)
            {
                return NotFound();
            }

            return mapaux;
        }

        // PUT: api/Producto/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProducto(int id, models.TablaProducto Producto)
        {
            if (id != Producto.IdProducto)
            {
                return BadRequest();
            }

            try
            {
                var mapaux = mapper.Map<models.TablaProducto, data.TablaProducto>(Producto);
                new BE.BS.Producto(dbcontext).Update(mapaux);
            }
            catch (Exception ee)
            {
                if (!ProductoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Producto
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<models.TablaProducto>> PostProducto(models.TablaProducto Producto)
        {
            var mapaux = mapper.Map<models.TablaProducto, data.TablaProducto>(Producto);
            new BE.BS.Producto(dbcontext).Insert(mapaux);

            return CreatedAtAction("GetProducto", new { id = Producto.IdProducto }, Producto);
        }

        // DELETE: api/Producto/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<models.TablaProducto>> DeleteProducto(int id)
        {
            var Producto = new BE.BS.Producto(dbcontext).GetOneById(id);
            if (Producto == null)
            {
                return NotFound();
            }

            new BE.BS.Producto(dbcontext).Delete(Producto);
            var mapaux = mapper.Map<data.TablaProducto, models.TablaProducto>(Producto);
            return mapaux;
        }

        private bool ProductoExists(int id)
        {
            return (new BE.BS.Producto(dbcontext).GetOneById(id) != null);
            //return _context.Producto.Any(e => e.FocusId == id);
        }
    }
}
