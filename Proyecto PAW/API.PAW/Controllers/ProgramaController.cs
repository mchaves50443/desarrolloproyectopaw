﻿using AutoMapper;
using data = BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models = API.PAW.Models;

namespace API.PAW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProgramaController : ControllerBase
    {
        private readonly DBContext dbcontext;
        private readonly IMapper mapper;
        public ProgramaController(DBContext _dbcontext, IMapper _mapper)
        {
            dbcontext = _dbcontext;
            mapper = _mapper;
        }

        // GET: api/TablaPrograma
        [HttpGet]
        public async Task<ActionResult<IEnumerable<models.TablaPrograma>>> GetTablaPrograma()
        {
            var res = new BE.BS.Programa(dbcontext).GetAll();
            var mapaux = mapper.Map<IEnumerable<data.TablaPrograma>, IEnumerable<models.TablaPrograma>>(res).ToList();
            return mapaux;
        }

        // GET: api/TablaPrograma/5
        [HttpGet("{id}")]
        public async Task<ActionResult<models.TablaPrograma>> GetTablaPrograma(long id)
        {
            var TablaPrograma = new BE.BS.Programa(dbcontext).GetOneById(id);

            if (TablaPrograma == null)
            {
                return NotFound();
            }
            var mapaux = mapper.Map<data.TablaPrograma, models.TablaPrograma>(TablaPrograma);
            return mapaux;
        }

        // PUT: api/TablaPrograma/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTablaPrograma(long id, models.TablaPrograma TablaPrograma)
        {
            if (id != TablaPrograma.IdPrograma)
            {
                return BadRequest();
            }

            try
            {
                var mapaux = mapper.Map<models.TablaPrograma, data.TablaPrograma>(TablaPrograma);
                new BE.BS.Programa(dbcontext).Update(mapaux);
            }
            catch (Exception ee)
            {
                if (!TablaProgramaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TablaPrograma
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<models.TablaPrograma>> PostTablaPrograma(models.TablaPrograma TablaPrograma)
        {
            var mapaux = mapper.Map<models.TablaPrograma, data.TablaPrograma>(TablaPrograma);
            new BE.BS.Programa(dbcontext).Insert(mapaux);

            return CreatedAtAction("GetTablaPrograma", new { id = TablaPrograma.IdPrograma }, TablaPrograma);
        }

        // DELETE: api/TablaPrograma/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<models.TablaPrograma>> DeleteTablaPrograma(int id)
        {
            var TablaPrograma = new BE.BS.Programa(dbcontext).GetOneById(id);
            if (TablaPrograma == null)
            {
                return NotFound();
            }

            new BE.BS.Programa(dbcontext).Delete(TablaPrograma);
            var mapaux = mapper.Map<data.TablaPrograma, models.TablaPrograma>(TablaPrograma);
            return mapaux;
        }

        private bool TablaProgramaExists(long id)
        {
            return (new BE.BS.Programa(dbcontext).GetOneById(id) != null);
        }
    }
}
