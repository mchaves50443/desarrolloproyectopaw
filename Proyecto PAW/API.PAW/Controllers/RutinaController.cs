﻿using AutoMapper;
using data = BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models = API.PAW.Models;

namespace API.PAW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RutinaController : ControllerBase
    {
        private readonly DBContext dbcontext;
        private readonly IMapper mapper;
        public RutinaController(DBContext _dbcontext, IMapper _mapper)
        {
            dbcontext = _dbcontext;
            mapper = _mapper;
        }

        // GET: api/Rutina
        [HttpGet]
        public async Task<ActionResult<IEnumerable<models.TablaRutina>>> GetRutina()
        {
            var res = await new BE.BS.Rutina(dbcontext).GetAllAsync();
            var mapaux = mapper.Map<IEnumerable<data.TablaRutina>, IEnumerable<models.TablaRutina>>(res).ToList();

            return mapaux;

        }

        // GET: api/Rutina/5
        [HttpGet("{id}")]
        public async Task<ActionResult<models.TablaRutina>> GetRutina(int id)
        {
            var Rutina = await new BE.BS.Rutina(dbcontext).GetOneByIdAsync(id);
            var mapaux = mapper.Map<data.TablaRutina, models.TablaRutina>(Rutina);


            if (Rutina == null)
            {
                return NotFound();
            }

            return mapaux;
        }

        // PUT: api/Rutina/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRutina(int id, models.TablaRutina Rutina)
        {
            if (id != Rutina.IdRutina)
            {
                return BadRequest();
            }

            try
            {
                var mapaux = mapper.Map<models.TablaRutina, data.TablaRutina>(Rutina);
                new BE.BS.Rutina(dbcontext).Update(mapaux);
            }
            catch (Exception ee)
            {
                if (!RutinaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Rutina
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<models.TablaRutina>> PostRutina(models.TablaRutina Rutina)
        {
            var mapaux = mapper.Map<models.TablaRutina, data.TablaRutina>(Rutina);
            new BE.BS.Rutina(dbcontext).Insert(mapaux);

            return CreatedAtAction("GetRutina", new { id = Rutina.IdRutina }, Rutina);
        }

        // DELETE: api/Rutina/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<models.TablaRutina>> DeleteRutina(int id)
        {
            var Rutina = new BE.BS.Rutina(dbcontext).GetOneById(id);
            if (Rutina == null)
            {
                return NotFound();
            }

            new BE.BS.Rutina(dbcontext).Delete(Rutina);
            var mapaux = mapper.Map<data.TablaRutina, models.TablaRutina>(Rutina);
            return mapaux;
        }

        private bool RutinaExists(int id)
        {
            return (new BE.BS.Rutina(dbcontext).GetOneById(id) != null);

        }
    }
}

