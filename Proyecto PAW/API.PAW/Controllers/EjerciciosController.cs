﻿using AutoMapper;
using data = BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models = API.PAW.Models;


namespace API.PAW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EjerciciosController : ControllerBase
    {
        private readonly DBContext dbcontext;
        private readonly IMapper mapper;
        public EjerciciosController(DBContext _dbcontext, IMapper _mapper)
        {
            dbcontext = _dbcontext;
            mapper = _mapper;
        }

        // GET: api/TablaEjercicios
        [HttpGet]
        public async Task<ActionResult<IEnumerable<models.TablaEjercicios>>> GetTablaEjercicios()
        {
            var res = new BE.BS.Ejercicios(dbcontext).GetAll();
            var mapaux = mapper.Map<IEnumerable<data.TablaEjercicios>, IEnumerable<models.TablaEjercicios>>(res).ToList();
            return mapaux;
        }

        // GET: api/TablaEjercicios/5
        [HttpGet("{id}")]
        public async Task<ActionResult<models.TablaEjercicios>> GetTablaEjercicios(long id)
        {
            var TablaEjercicios = new BE.BS.Ejercicios(dbcontext).GetOneById(id);

            if (TablaEjercicios == null)
            {
                return NotFound();
            }
            var mapaux = mapper.Map<data.TablaEjercicios, models.TablaEjercicios>(TablaEjercicios);
            return mapaux;
        }

        // PUT: api/TablaEjercicios/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTablaEjercicios(long id, models.TablaEjercicios TablaEjercicios)
        {
            if (id != TablaEjercicios.IdEjercicio)
            {
                return BadRequest();
            }

            try
            {
                var mapaux = mapper.Map<models.TablaEjercicios, data.TablaEjercicios>(TablaEjercicios);
                new BE.BS.Ejercicios(dbcontext).Update(mapaux);
            }
            catch (Exception ee)
            {
                if (!TablaEjerciciosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TablaEjercicios
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<models.TablaEjercicios>> PostTablaEjercicios(models.TablaEjercicios TablaEjercicios)
        {
            var mapaux = mapper.Map<models.TablaEjercicios, data.TablaEjercicios>(TablaEjercicios);
            new BE.BS.Ejercicios(dbcontext).Insert(mapaux);

            return CreatedAtAction("GetTablaEjercicios", new { id = TablaEjercicios.IdEjercicio }, TablaEjercicios);
        }

        // DELETE: api/TablaEjercicios/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<models.TablaEjercicios>> DeleteTablaEjercicios(long id)
        {
            var TablaEjercicios = new BE.BS.Ejercicios(dbcontext).GetOneById(id);
            if (TablaEjercicios == null)
            {
                return NotFound();
            }

            new BE.BS.Ejercicios(dbcontext).Delete(TablaEjercicios);
            var mapaux = mapper.Map<data.TablaEjercicios, models.TablaEjercicios>(TablaEjercicios);
            return mapaux;
        }

        private bool TablaEjerciciosExists(long id)
        {
            return (new BE.BS.Ejercicios(dbcontext).GetOneById(id) != null);
        }
    }
}
