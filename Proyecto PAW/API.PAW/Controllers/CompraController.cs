﻿using AutoMapper;
using data = BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models = API.PAW.Models;

namespace API.PAW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompraController : ControllerBase
    {
        private readonly DBContext dbcontext;
        private readonly IMapper mapper;
        public CompraController(DBContext _dbcontext, IMapper _mapper)
        {
            dbcontext = _dbcontext;
            mapper = _mapper;
        }

        // GET: api/Compra
        [HttpGet]
        public async Task<ActionResult<IEnumerable<models.TablaCompra>>> GetCompra()
        {
            var res = await new BE.BS.Compra(dbcontext).GetAllAsync();
            var mapaux = mapper.Map<IEnumerable<data.TablaCompra>, IEnumerable<models.TablaCompra>>(res).ToList();

            return mapaux;

        }

        // GET: api/Compra/5
        [HttpGet("{id}")]
        public async Task<ActionResult<models.TablaCompra>> GetCompra(int id)
        {
            var Compra = await new BE.BS.Compra(dbcontext).GetOneByIdAsync(id);
            var mapaux = mapper.Map<data.TablaCompra, models.TablaCompra>(Compra);


            if (Compra == null)
            {
                return NotFound();
            }

            return mapaux;
        }

        // PUT: api/Compra/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompra(int id, models.TablaCompra Compra)
        {
            if (id != Compra.IdCompra)
            {
                return BadRequest();
            }

            try
            {
                var mapaux = mapper.Map<models.TablaCompra, data.TablaCompra>(Compra);
                new BE.BS.Compra(dbcontext).Update(mapaux);
            }
            catch (Exception ee)
            {
                if (!CompraExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Compra
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<models.TablaCompra>> PostCompra(models.TablaCompra Compra)
        {
            var mapaux = mapper.Map<models.TablaCompra, data.TablaCompra>(Compra);
            new BE.BS.Compra(dbcontext).Insert(mapaux);

            return CreatedAtAction("GetCompra", new { id = Compra.IdCompra }, Compra);
        }

        // DELETE: api/Compra/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<models.TablaCompra>> DeleteCompra(int id)
        {
            var Compra = new BE.BS.Compra(dbcontext).GetOneById(id);
            if (Compra == null)
            {
                return NotFound();
            }

            new BE.BS.Compra(dbcontext).Delete(Compra);
            var mapaux = mapper.Map<data.TablaCompra, models.TablaCompra>(Compra);
            return mapaux;
        }

        private bool CompraExists(int id)
        {
            return (new BE.BS.Compra(dbcontext).GetOneById(id) != null);

        }
    }
}

