﻿using AutoMapper;
using data = BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models = API.PAW.Models;

namespace API.PAW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly DBContext dbcontext;
        private readonly IMapper mapper;
        public UsuarioController(DBContext _dbcontext, IMapper _mapper)
        {
            dbcontext = _dbcontext;
            mapper = _mapper;
        }

        // GET: api/TablaUsuario
        [HttpGet]
        public async Task<ActionResult<IEnumerable<models.TablaUsuario>>> GetTablaUsuario()
        {
            var res = new BE.BS.Usuario(dbcontext).GetAll();
            var mapaux = mapper.Map<IEnumerable<data.TablaUsuario>, IEnumerable<models.TablaUsuario>>(res).ToList();
            return mapaux;
        }

        // GET: api/TablaUsuario/5
        [HttpGet("{id}")]
        public async Task<ActionResult<models.TablaUsuario>> GetTablaUsuario(long id)
        {
            var TablaUsuario = new BE.BS.Usuario(dbcontext).GetOneById(id);

            if (TablaUsuario == null)
            {
                return NotFound();
            }
            var mapaux = mapper.Map<data.TablaUsuario, models.TablaUsuario>(TablaUsuario);
            return mapaux;
        }


        // GET: api/TablaUsuarioLogin/5
        [HttpGet("login/{user}/{password}")]
        public async Task<ActionResult<models.TablaUsuario>> GetTablaUsuarioLogin(string user, string password)
        {

            var TablaUsuario = new BE.BS.Usuario(dbcontext).GetLogin(user,password);

            if (TablaUsuario == null)
            {
                return NotFound();
            }
            var mapaux = mapper.Map<data.TablaUsuario, models.TablaUsuario>(TablaUsuario);
            return mapaux;
        }

        // PUT: api/TablaUsuario/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTablaUsuario(long id, models.TablaUsuario TablaUsuario)
        {
            if (id != TablaUsuario.IdUsuario)
            {
                return BadRequest();
            }

            try
            {
                var mapaux = mapper.Map<models.TablaUsuario, data.TablaUsuario>(TablaUsuario);
                new BE.BS.Usuario(dbcontext).Update(mapaux);
            }
            catch (Exception ee)
            {
                if (!TablaUsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TablaUsuario
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<models.TablaUsuario>> PostTablaUsuario(models.TablaUsuario TablaUsuario)
        {
            var mapaux = mapper.Map<models.TablaUsuario, data.TablaUsuario>(TablaUsuario);
            new BE.BS.Usuario(dbcontext).Insert(mapaux);

            return CreatedAtAction("GetTablaUsuario", new { id = TablaUsuario.IdUsuario }, TablaUsuario);
        }

        // DELETE: api/TablaUsuario/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<models.TablaUsuario>> DeleteTablaUsuario(int id)
        {
            var TablaUsuario = new BE.BS.Usuario(dbcontext).GetOneById(id);
            if (TablaUsuario == null)
            {
                return NotFound();
            }

            new BE.BS.Usuario(dbcontext).Delete(TablaUsuario);
            var mapaux = mapper.Map<data.TablaUsuario, models.TablaUsuario>(TablaUsuario);
            return mapaux;
        }

        private bool TablaUsuarioExists(long id)
        {
            return (new BE.BS.Usuario(dbcontext).GetOneById(id) != null);
        }
    }
}
