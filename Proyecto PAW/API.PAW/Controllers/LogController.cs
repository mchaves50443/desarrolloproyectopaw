﻿using AutoMapper;
using data = BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models = API.PAW.Models;

namespace API.PAW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private readonly DBContext dbcontext;
        private readonly IMapper mapper;
        public LogController(DBContext _dbcontext, IMapper _mapper)
        {
            dbcontext = _dbcontext;
            mapper = _mapper;
        }

        // GET: api/Log
        [HttpGet]
        public async Task<ActionResult<IEnumerable<models.TablaLog>>> GetLog()
        {
            var res = await new BE.BS.Log(dbcontext).GetAllAsync();
            var mapaux = mapper.Map<IEnumerable<data.TablaLog>, IEnumerable<models.TablaLog>>(res).ToList();

            return mapaux;

            // Este GetAll no trae las relaaciones
            //return new BE.BS.Log(dbcontext).GetAll().ToList();
        }

        // GET: api/Log/5
        [HttpGet("{id}")]
        public async Task<ActionResult<models.TablaLog>> GetLog(int id)
        {
            var Log = await new BE.BS.Log(dbcontext).GetOneByIdAsync(id);
            var mapaux = mapper.Map<data.TablaLog, models.TablaLog>(Log);

            // Este Get no trae las relaaciones
            //var Log = new BE.BS.Log(dbcontext).GetOneById(id);

            if (Log == null)
            {
                return NotFound();
            }

            return mapaux;
        }

        // PUT: api/Log/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLog(int id, models.TablaLog Log)
        {
            if (id != Log.IdLog)
            {
                return BadRequest();
            }

            try
            {
                var mapaux = mapper.Map<models.TablaLog, data.TablaLog>(Log);
                new BE.BS.Log(dbcontext).Update(mapaux);
            }
            catch (Exception ee)
            {
                if (!LogExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Log
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<models.TablaLog>> PostLog(models.TablaLog Log)
        {
            var mapaux = mapper.Map<models.TablaLog, data.TablaLog>(Log);
            new BE.BS.Log(dbcontext).Insert(mapaux);

            return CreatedAtAction("GetLog", new { id = Log.IdLog }, Log);
        }

        // DELETE: api/Log/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<models.TablaLog>> DeleteLog(int id)
        {
            var Log = new BE.BS.Log(dbcontext).GetOneById(id);
            if (Log == null)
            {
                return NotFound();
            }

            new BE.BS.Log(dbcontext).Delete(Log);
            var mapaux = mapper.Map<data.TablaLog, models.TablaLog>(Log);
            return mapaux;
        }

        private bool LogExists(int id)
        {
            return (new BE.BS.Log(dbcontext).GetOneById(id) != null);
            //return _context.Log.Any(e => e.FocusId == id);
        }
    }
}
