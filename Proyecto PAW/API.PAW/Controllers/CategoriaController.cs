﻿using AutoMapper;
using data = BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using models = API.PAW.Models;

namespace API.PAW.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : ControllerBase
    {
        private readonly DBContext dbcontext;
        private readonly IMapper mapper;
        public CategoriaController(DBContext _dbcontext, IMapper _mapper)
        {
            dbcontext = _dbcontext;
            mapper = _mapper;
        }

        // GET: api/TablaCategoria
        [HttpGet]
        public async Task<ActionResult<IEnumerable<models.TablaCategoria>>> GetTablaCategoria()
        {
            var res = new BE.BS.Categoria(dbcontext).GetAll();
            var mapaux = mapper.Map<IEnumerable<data.TablaCategoria>, IEnumerable<models.TablaCategoria>>(res).ToList();
            return mapaux;
        }

        // GET: api/TablaCategoria/5
        [HttpGet("{id}")]
        public async Task<ActionResult<models.TablaCategoria>> GetTablaCategoria(long id)
        {
            var TablaCategoria = new BE.BS.Categoria(dbcontext).GetOneById(id);

            if (TablaCategoria == null)
            {
                return NotFound();
            }
            var mapaux = mapper.Map<data.TablaCategoria, models.TablaCategoria>(TablaCategoria);
            return mapaux;
        }

        // PUT: api/TablaCategoria/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTablaCategoria(long id, models.TablaCategoria TablaCategoria)
        {
            if (id != TablaCategoria.IdCategoria)
            {
                return BadRequest();
            }

            try
            {
                var mapaux = mapper.Map<models.TablaCategoria, data.TablaCategoria>(TablaCategoria);
                new BE.BS.Categoria(dbcontext).Update(mapaux);
            }
            catch (Exception ee)
            {
                if (!TablaCategoriaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TablaCategoria
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<models.TablaCategoria>> PostTablaCategoria(models.TablaCategoria TablaCategoria)
        {
            var mapaux = mapper.Map<models.TablaCategoria, data.TablaCategoria>(TablaCategoria);
            new BE.BS.Categoria(dbcontext).Insert(mapaux);

            return CreatedAtAction("GetTablaCategoria", new { id = TablaCategoria.IdCategoria }, TablaCategoria);
        }

        // DELETE: api/TablaCategoria/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<models.TablaCategoria>> DeleteTablaCategoria(long id)
        {
            var TablaCategoria = new BE.BS.Categoria(dbcontext).GetOneById(id);
            if (TablaCategoria == null)
            {
                return NotFound();
            }

            new BE.BS.Categoria(dbcontext).Delete(TablaCategoria);
            var mapaux = mapper.Map<data.TablaCategoria, models.TablaCategoria>(TablaCategoria);
            return mapaux;
        }

        private bool TablaCategoriaExists(long id)
        {
            return (new BE.BS.Categoria(dbcontext).GetOneById(id) != null);
        }
    }
}
