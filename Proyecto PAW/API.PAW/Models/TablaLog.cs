﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.PAW.Models
{
    public class TablaLog
    {
        public long IdLog { get; set; }
        public long? IdUsuario { get; set; }
        public long? IdActividad { get; set; }
        public DateTime? Fecha { get; set; }

        public virtual TablaActividades IdActividadNavigation { get; set; }
        public virtual TablaUsuario IdUsuarioNavigation { get; set; }
    }
}
