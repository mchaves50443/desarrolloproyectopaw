﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.PAW.Models
{
    public class TablaPrograma
    {
        public TablaPrograma()
        {
        }

        public long IdPrograma { get; set; }
        public string TituloPrograma { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
        public long? CantVentas { get; set; }

    }
}
