﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.PAW.Models
{
    public class TablaProgramaEjercicio
    {
        public long IdProgramaEjercicio { get; set; }
        public long FkIdPrograma { get; set; }
        public long FkIdEjercicio { get; set; }
        public short NSets { get; set; }
        public short NReps { get; set; }
        public string Notas { get; set; }

        public virtual TablaEjercicios FkIdEjercicioNavigation { get; set; }
        public virtual TablaPrograma FkIdProgramaNavigation { get; set; }
    }
}
