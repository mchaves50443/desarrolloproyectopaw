﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.PAW.Models
{
    public class TablaRutina
    {
        public long IdRutina { get; set; }
        public long IdUsuario { get; set; }
        public long IdPrograma { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }

        public virtual TablaPrograma IdProgramaNavigation { get; set; }
        public virtual TablaUsuario IdUsuarioNavigation { get; set; }
    }
}
