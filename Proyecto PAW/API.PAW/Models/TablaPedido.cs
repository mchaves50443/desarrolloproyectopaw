﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.PAW.Models
{
    public class TablaPedido
    {
        public long IdPedido { get; set; }
        public long IdProducto { get; set; }
        public int Cantidad { get; set; }
        public DateTime FechaPedido { get; set; }
        public DateTime FechaEntrega { get; set; }
        public decimal? Total { get; set; }
        public virtual TablaProducto IdProductoNavigation { get; set; }
    }
}
