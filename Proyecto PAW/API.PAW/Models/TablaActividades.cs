﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.PAW.Models
{
    public class TablaActividades
    {
        public long IdActividad { get; set; }
        public string NombreActivIdAdes { get; set; }
        public decimal Precio { get; set; }
        public DateTime Fecha { get; set; }
        public string Descripcion { get; set; }
        public short TipoActividad { get; set; }
        public short MinInventario { get; set; }
        public short MaxInventario { get; set; }
    }
}
