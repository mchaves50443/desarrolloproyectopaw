﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.PAW.Models
{
    public class TablaEjercicios
    {
        public TablaEjercicios()
        {
        }

        public long IdEjercicio { get; set; }
        public string NombreEjercicio { get; set; }
        public string LinkVideo { get; set; }

    }
}
