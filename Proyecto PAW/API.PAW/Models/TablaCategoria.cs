﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.PAW.Models
{
    public class TablaCategoria
    {
        public long IdCategoria { get; set; }
        public string NombreCategoria { get; set; }
    }
}
