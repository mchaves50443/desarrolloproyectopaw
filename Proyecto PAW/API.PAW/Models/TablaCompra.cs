﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.PAW.Models
{
    public class TablaCompra
    {
        public long IdCompra { get; set; }
        public long IdUsuario { get; set; }
        public long IdProducto { get; set; }
        public int Cantidad { get; set; }
        public decimal Total { get; set; }
        public DateTime Fecha { get; set; }

        public virtual TablaProducto IdProductoNavigation { get; set; }
        public virtual TablaUsuario IdUsuarioNavigation { get; set; }
    }
}
