﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.PAW.Models
{
    public class TablaProducto
    {
        public long IdProducto { get; set; }
        public string NombreProducto { get; set; }
        public string Descripcion { get; set; }
        public int CantInventario { get; set; }
        public decimal Precio { get; set; }
        public long? IdCategoria { get; set; }
        public long? IdProveedor { get; set; }

        public virtual TablaCategoria IdCategoriaNavigation { get; set; }
        public virtual TablaProveedor IdProveedorNavigation { get; set; }
    }
}
