﻿using BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;

namespace BE.DAL.Repository
{
    public class RepositoryProducto : Repository<data.TablaProducto>, IRepositoryProducto
    {
        public RepositoryProducto(DBContext _dbContext) : base(_dbContext)
        {

        }

        public async Task<IEnumerable<TablaProducto>> GetAllAsync()
        {
            return await _db.TablaProducto
                .Include(m => m.IdCategoriaNavigation)
                .Include(z => z.IdProveedorNavigation)
                .ToListAsync();
        }

        public async Task<TablaProducto> GetOneByIdAsync(long IdProducto)
        {
            return await _db.TablaProducto
                .Include(m => m.IdCategoriaNavigation)
                .Include(z => z.IdProveedorNavigation)
                .SingleOrDefaultAsync(m => m.IdProducto == IdProducto);
        }


        private DBContext _db
        {
            get { return dbContext as DBContext; }
        }
    }
}
