﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;

namespace BE.DAL.Repository
{
    public interface IRepositoryRutina: IRepository<data.TablaRutina>
    {
        Task<IEnumerable<data.TablaRutina>> GetAllAsync();
        Task<data.TablaRutina> GetOneByIdAsync(long IdRutina);
    }
}
