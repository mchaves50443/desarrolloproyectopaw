﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;

namespace BE.DAL.Repository
{
    public interface IRepositoryProducto : IRepository<data.TablaProducto>
    {
        Task<IEnumerable<data.TablaProducto>> GetAllAsync();
        Task<data.TablaProducto> GetOneByIdAsync(long IdProducto);
    }
}

