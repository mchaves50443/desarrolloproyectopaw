﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;

namespace BE.DAL.Repository
{
    public interface IRepositoryProgramaEjercicio : IRepository<data.TablaProgramaEjercicio>
    {
        Task<IEnumerable<data.TablaProgramaEjercicio>> GetAllAsync();
        Task<data.TablaProgramaEjercicio> GetOneByIdAsync(long IdProgramaEjercicio);
    }
}
