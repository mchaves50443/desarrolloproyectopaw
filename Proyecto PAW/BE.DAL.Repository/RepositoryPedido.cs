﻿using BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;

namespace BE.DAL.Repository
{
    public class RepositoryPedido : Repository<data.TablaPedido>, IRepositoryPedido
    {
        public RepositoryPedido(DBContext _dbContext) : base(_dbContext)
        {

        }

        public async Task<IEnumerable<TablaPedido>> GetAllAsync()
        {
            return await _db.TablaPedido
                .Include(m => m.IdProductoNavigation)
                
                .ToListAsync();
        }

        public async Task<TablaPedido> GetOneByIdAsync(long IdPedido)
        {
            return await _db.TablaPedido
                .Include(m => m.IdProductoNavigation)

                .SingleOrDefaultAsync(m => m.IdPedido == IdPedido);
        }


        private DBContext _db
        {
            get { return dbContext as DBContext; }
        }
    }
}
