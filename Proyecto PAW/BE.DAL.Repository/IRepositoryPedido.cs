﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;

namespace BE.DAL.Repository
{
   public  interface IRepositoryPedido : IRepository<data.TablaPedido>
    {
        Task<IEnumerable<data.TablaPedido>> GetAllAsync();
        Task<data.TablaPedido> GetOneByIdAsync(long IdPedido);
    }
}