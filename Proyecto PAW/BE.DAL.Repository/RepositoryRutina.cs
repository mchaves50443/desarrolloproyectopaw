﻿using BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;

namespace BE.DAL.Repository
{
    public class RepositoryRutina: Repository<data.TablaRutina>, IRepositoryRutina
    {
        public RepositoryRutina(DBContext _dbContext) : base(_dbContext)
        {

        }

        public async Task<IEnumerable<TablaRutina>> GetAllAsync()
        {
            return await _db.TablaRutina
                .Include(m => m.IdUsuarioNavigation)
                
                .Include(z => z.IdProgramaNavigation)
           

                .ToListAsync();
        }

        public async Task<TablaRutina> GetOneByIdAsync(long IdRutina)
        {
            return await _db.TablaRutina
                 .Include(m => m.IdUsuarioNavigation)

                .Include(z => z.IdProgramaNavigation)

                .SingleOrDefaultAsync(m => m.IdRutina == IdRutina);
        }


        private DBContext _db
        {
            get { return dbContext as DBContext; }
        }
    }
}