﻿using BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;


namespace BE.DAL.Repository
{
    public class RepositoryProgramaEjercicio : Repository<data.TablaProgramaEjercicio>, IRepositoryProgramaEjercicio
    {
        public RepositoryProgramaEjercicio(DBContext _dbContext) : base(_dbContext)
        {

        }

        public async Task<IEnumerable<TablaProgramaEjercicio>> GetAllAsync()
        {
            return await _db.TablaProgramaEjercicio
                .Include(m => m.FkIdProgramaNavigation)
                .Include(z => z.FkIdEjercicioNavigation)
                .ToListAsync();
        }

        public async Task<TablaProgramaEjercicio> GetOneByIdAsync(long IdProgramaEjercicio)
        {
            return await _db.TablaProgramaEjercicio
                .Include(m => m.FkIdProgramaNavigation)
                .Include(m => m.FkIdEjercicioNavigation)
                .SingleOrDefaultAsync(m => m.IdProgramaEjercicio == IdProgramaEjercicio);
        }

        

        public async void DeleteAsync(data.TablaProgramaEjercicio objeto)
        {
            objeto = _db.TablaProgramaEjercicio.Where(d => d.FkIdPrograma == objeto.FkIdPrograma && d.FkIdEjercicio == objeto.FkIdEjercicio).First();
            _db.TablaProgramaEjercicio.Remove(objeto);

        }

        public async void UpdateAsync(data.TablaProgramaEjercicio objeto)
        {
            _db.TablaProgramaEjercicio.Update(objeto);
        }

        private DBContext _db
        {
            get { return dbContext as DBContext; }
        }
    }
}
