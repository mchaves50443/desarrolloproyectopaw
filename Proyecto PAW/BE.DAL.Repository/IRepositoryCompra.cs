﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;

namespace BE.DAL.Repository
{
    public interface IRepositoryCompra : IRepository<data.TablaCompra>
    {
        Task<IEnumerable<data.TablaCompra>> GetAllAsync();
        Task<data.TablaCompra> GetOneByIdAsync(long IdComopra);
    }
}
