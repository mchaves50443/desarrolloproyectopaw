﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;

namespace BE.DAL.Repository
{
    public interface IRepositoryLog : IRepository<data.TablaLog>
    {
        Task<IEnumerable<data.TablaLog>> GetAllAsync();
        Task<data.TablaLog> GetOneByIdAsync(long IdLog);
    }
}