﻿using BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;

namespace BE.DAL.Repository
{
    public class RepositoryCompra : Repository<data.TablaCompra>, IRepositoryCompra
    {
        public RepositoryCompra(DBContext _dbContext) : base(_dbContext)
        {

        }

        public async Task<IEnumerable<TablaCompra>> GetAllAsync()
        {
            return await _db.TablaCompra
                .Include(m => m.IdUsuarioNavigation)
                
                .Include(z => z.IdProductoNavigation)
           

                .ToListAsync();
        }

        public async Task<TablaCompra> GetOneByIdAsync(long IdCompra)
        {
            return await _db.TablaCompra
                 .Include(m => m.IdUsuarioNavigation)

                .Include(z => z.IdProductoNavigation)

                .SingleOrDefaultAsync(m => m.IdCompra == IdCompra);
        }


        private DBContext _db
        {
            get { return dbContext as DBContext; }
        }
    }
}