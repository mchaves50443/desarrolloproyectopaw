﻿using BE.DAL.DO.Objects;
using BE.DAL.FE;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;

namespace BE.DAL.Repository
{
    public class RepositoryLog : Repository<data.TablaLog>, IRepositoryLog
    {
        public RepositoryLog(DBContext _dbContext) : base(_dbContext)
        {

        }

        public async Task<IEnumerable<TablaLog>> GetAllAsync()
        {
            return await _db.TablaLog
                .Include(m => m.IdActividadNavigation)
                .Include(z => z.IdUsuarioNavigation)

                .ToListAsync();
        }

        public async Task<TablaLog> GetOneByIdAsync(long IdLog)
        {
            return await _db.TablaLog
                .Include(m => m.IdActividadNavigation)
                .Include(z => z.IdUsuarioNavigation)

                .SingleOrDefaultAsync(m => m.IdLog == IdLog);
        }


        private DBContext _db
        {
            get { return dbContext as DBContext; }
        }
    }
}