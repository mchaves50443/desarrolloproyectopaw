﻿using System;
using System.Collections.Generic;
using System.Text;
using data = BE.DAL.DO.Objects;
using dal = BE.DAL;
using BE.DAL;
using BE.DAL.FE;
using BE.DAL.DO.Interfaces;
using System.Threading.Tasks;

namespace BE.BS
{
   public  class Pedido : ICRUD<data.TablaPedido>
    {
        private dal.Pedido _dal;
        public Pedido(DBContext dbContext)
        {
            _dal = new dal.Pedido(dbContext);
        }
        public void Delete(data.TablaPedido t)
        {
            _dal.Delete(t);
        }

        public IEnumerable<data.TablaPedido> GetAll()
        {
            return null;
        }

        public Task<IEnumerable<data.TablaPedido>> GetAllAsync()
        {
            return _dal.GetAllAsync();
        }

        public data.TablaPedido GetOneById(long id)
        {
            return _dal.GetOneById(id);
        }

        public Task<data.TablaPedido> GetOneByIdAsync(long id)
        {
            return _dal.GetOneByIdAsync(id);
        }

        public void Insert(data.TablaPedido t)
        {
            _dal.Insert(t);
        }

        public void Update(data.TablaPedido t)
        {
            _dal.Update(t);
        }
    }
}
