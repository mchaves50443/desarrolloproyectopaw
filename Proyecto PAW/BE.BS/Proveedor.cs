﻿using BE.DAL.DO.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;
using dal = BE.DAL;
using BE.DAL.FE;

namespace BE.BS
{
    public class Proveedor : ICRUD<data.TablaProveedor>
    {
        private dal.Proveedor _dal;
        public Proveedor(DBContext dbContext)
        {
            _dal = new dal.Proveedor(dbContext);
        }
        public void Delete(data.TablaProveedor t)
        {
            _dal.Delete(t);
        }

        public IEnumerable<data.TablaProveedor> GetAll()
        {
            return _dal.GetAll();
        }

        public Task<IEnumerable<data.TablaProveedor>> GetAllAsync()
        {
            return _dal.GetAllAsync();
        }

        public data.TablaProveedor GetOneById(long id)
        {
            return _dal.GetOneById(id);
        }

        public Task<data.TablaProveedor> GetOneByIdAsync(long id)
        {
            return _dal.GetOneByIdAsync(id);
        }

        public void Insert(data.TablaProveedor t)
        {
            _dal.Insert(t);
        }

        public void Update(data.TablaProveedor t)
        {
            _dal.Update(t);
        }
    }
}