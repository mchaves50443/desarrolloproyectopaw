﻿using System;
using System.Collections.Generic;
using System.Text;
using data = BE.DAL.DO.Objects;
using dal = BE.DAL;
using BE.DAL;
using BE.DAL.FE;
using BE.DAL.DO.Interfaces;
using System.Threading.Tasks;

namespace BE.BS
{
    public class Producto : ICRUD<data.TablaProducto>
    {
        private dal.Producto _dal;
        public Producto(DBContext dbContext)
        {
            _dal = new dal.Producto(dbContext);
        }
        public void Delete(data.TablaProducto t)
        {
            _dal.Delete(t);
        }

        public IEnumerable<data.TablaProducto> GetAll()
        {
            return null;
        }

        public Task<IEnumerable<data.TablaProducto>> GetAllAsync()
        {
            return _dal.GetAllAsync();
        }

        public data.TablaProducto GetOneById(long id)
        {
            return _dal.GetOneById(id);
        }

        public Task<data.TablaProducto> GetOneByIdAsync(long id)
        {
            return _dal.GetOneByIdAsync(id);
        }

        public void Insert(data.TablaProducto t)
        {
            _dal.Insert(t);
        }

        public void Update(data.TablaProducto t)
        {
            _dal.Update(t);
        }
    }
}
