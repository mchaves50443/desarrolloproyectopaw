﻿using System;
using System.Collections.Generic;
using System.Text;
using data = BE.DAL.DO.Objects;
using dal = BE.DAL;
using BE.DAL;
using BE.DAL.FE;
using BE.DAL.DO.Interfaces;
using System.Threading.Tasks;

namespace BE.BS
{
    public class Log : ICRUD<data.TablaLog>
    {
        private dal.Log _dal;
        public Log(DBContext dbContext)
        {
            _dal = new dal.Log(dbContext);
        }
        public void Delete(data.TablaLog t)
        {
            _dal.Delete(t);
        }

        public IEnumerable<data.TablaLog> GetAll()
        {
            return null;
        }

        public Task<IEnumerable<data.TablaLog>> GetAllAsync()
        {
            return _dal.GetAllAsync();
        }

        public data.TablaLog GetOneById(long id)
        {
            return _dal.GetOneById(id);
        }

        public Task<data.TablaLog> GetOneByIdAsync(long id)
        {
            return _dal.GetOneByIdAsync(id);
        }

        public void Insert(data.TablaLog t)
        {
            _dal.Insert(t);
        }

        public void Update(data.TablaLog t)
        {
            _dal.Update(t);
        }
    }
}
