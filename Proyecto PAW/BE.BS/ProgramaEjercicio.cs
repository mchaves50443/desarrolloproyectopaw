﻿using System;
using System.Collections.Generic;
using System.Text;
using data = BE.DAL.DO.Objects;
using dal = BE.DAL;
using BE.DAL;
using BE.DAL.FE;
using BE.DAL.DO.Interfaces;
using System.Threading.Tasks;
using BE.DAL.DO.Objects;

namespace BE.BS
{
    public class ProgramaEjercicio : ICRUD<data.TablaProgramaEjercicio>
    {
        private dal.ProgramaEjercicio _dal;
        public ProgramaEjercicio(DBContext dbContext)
        {
            _dal = new dal.ProgramaEjercicio(dbContext);
        }
        public void Delete(data.TablaProgramaEjercicio t)
        {
            _dal.Delete(t);
        }

        public IEnumerable<data.TablaProgramaEjercicio> GetAll()
        {
            return _dal.GetAll();
        }

        public Task<IEnumerable<data.TablaProgramaEjercicio>> GetAllAsync()
        {
            return _dal.GetAllAsync();
        }

        public data.TablaProgramaEjercicio GetOneById(long id)
        {
            return _dal.GetOneById(id);
        }

        public Task<data.TablaProgramaEjercicio> GetOneByIdAsync(long id)
        {
            return _dal.GetOneByIdAsync(id);
        }

        

        public void Insert(data.TablaProgramaEjercicio t)
        {
            _dal.Insert(t);
        }

        public void Update(data.TablaProgramaEjercicio t)
        {
            _dal.Update(t);
        }

       
    }
}
