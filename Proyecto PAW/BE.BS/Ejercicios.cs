﻿using BE.DAL.DO.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;
using dal = BE.DAL;
using BE.DAL.FE;

namespace BE.BS
{
    public class Ejercicios : ICRUD<data.TablaEjercicios>
    {
        private dal.Ejercicios _dal;
        public Ejercicios(DBContext dbContext)
        {
            _dal = new dal.Ejercicios(dbContext);
        }
        public void Delete(data.TablaEjercicios t)
        {
            _dal.Delete(t);
        }

        public IEnumerable<data.TablaEjercicios> GetAll()
        {
            return _dal.GetAll();
        }

        public Task<IEnumerable<data.TablaEjercicios>> GetAllAsync()
        {
            return _dal.GetAllAsync();
        }

        public data.TablaEjercicios GetOneById(long id)
        {
            return _dal.GetOneById(id);
        }

        public Task<data.TablaEjercicios> GetOneByIdAsync(long id)
        {
            return _dal.GetOneByIdAsync(id);
        }

        public void Insert(data.TablaEjercicios t)
        {
            _dal.Insert(t);
        }

        public void Update(data.TablaEjercicios t)
        {
            _dal.Update(t);
        }
    }
}
