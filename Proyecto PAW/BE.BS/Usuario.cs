﻿using BE.DAL.DO.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;
using dal = BE.DAL;
using BE.DAL.FE;

namespace BE.BS
{
    public class Usuario : ICRUD<data.TablaUsuario>
    {
        private dal.Usuario _dal;
        public Usuario(DBContext dbContext)
        {
            _dal = new dal.Usuario(dbContext);
        }
        public void Delete(data.TablaUsuario t)
        {
            _dal.Delete(t);
        }

        public IEnumerable<data.TablaUsuario> GetAll()
        {
            return _dal.GetAll();
        }

        public Task<IEnumerable<data.TablaUsuario>> GetAllAsync()
        {
            return _dal.GetAllAsync();
        }

        public data.TablaUsuario GetOneById(long id)
        {
            return _dal.GetOneById(id);
        }

        public data.TablaUsuario GetLogin(string user, string password)
        {
            return _dal.GetLogin(user,password);
        }

        public Task<data.TablaUsuario> GetOneByIdAsync(long id)
        {
            return _dal.GetOneByIdAsync(id);
        }

        public void Insert(data.TablaUsuario t)
        {
            _dal.Insert(t);
        }

        public void Update(data.TablaUsuario t)
        {
            _dal.Update(t);
        }
    }
}
