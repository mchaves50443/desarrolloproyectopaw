﻿using BE.DAL.DO.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;
using dal = BE.DAL;
using BE.DAL.FE;

namespace BE.BS
{
    public class Actividades : ICRUD<data.TablaActividades>
    {
        private dal.Actividades _dal;
        public Actividades(DBContext dbContext)
        {
            _dal = new dal.Actividades(dbContext);
        }
        public void Delete(data.TablaActividades t)
        {
            _dal.Delete(t);
        }

        public IEnumerable<data.TablaActividades> GetAll()
        {
            return _dal.GetAll();
        }

        public Task<IEnumerable<data.TablaActividades>> GetAllAsync()
        {
            return _dal.GetAllAsync();
        }

        public data.TablaActividades GetOneById(long id)
        {
            return _dal.GetOneById(id);
        }

        public Task<data.TablaActividades> GetOneByIdAsync(long id)
        {
            return _dal.GetOneByIdAsync(id);
        }

        public void Insert(data.TablaActividades t)
        {
            _dal.Insert(t);
        }

        public void Update(data.TablaActividades t)
        {
            _dal.Update(t);
        }
    }
}
