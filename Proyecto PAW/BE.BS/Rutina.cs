﻿using System;
using System.Collections.Generic;
using System.Text;
using data = BE.DAL.DO.Objects;
using dal = BE.DAL;
using BE.DAL;
using BE.DAL.FE;
using BE.DAL.DO.Interfaces;
using System.Threading.Tasks;

namespace BE.BS
{
    public class Rutina : ICRUD<data.TablaRutina>
    {
        private dal.Rutina _dal;
        public Rutina(DBContext dbContext)
        {
            _dal = new dal.Rutina(dbContext);
        }
        public void Delete(data.TablaRutina t)
        {
            _dal.Delete(t);
        }

        public IEnumerable<data.TablaRutina> GetAll()
        {
            return null;
        }

        public Task<IEnumerable<data.TablaRutina>> GetAllAsync()
        {
            return _dal.GetAllAsync();
        }

        public data.TablaRutina GetOneById(long id)
        {
            return _dal.GetOneById(id);
        }

        public Task<data.TablaRutina> GetOneByIdAsync(long id)
        {
            return _dal.GetOneByIdAsync(id);
        }

        public void Insert(data.TablaRutina t)
        {
            _dal.Insert(t);
        }

        public void Update(data.TablaRutina t)
        {
            _dal.Update(t);
        }
    }
}
