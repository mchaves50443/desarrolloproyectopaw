﻿using BE.DAL.DO.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;
using dal = BE.DAL;
using BE.DAL.FE;


namespace BE.BS
{
    public class Programa : ICRUD<data.TablaPrograma>
    {
        private dal.Programa _dal;
        public Programa(DBContext dbContext)
        {
            _dal = new dal.Programa(dbContext);
        }
        public void Delete(data.TablaPrograma t)
        {
            _dal.Delete(t);
        }

        public IEnumerable<data.TablaPrograma> GetAll()
        {
            return _dal.GetAll();
        }

        public Task<IEnumerable<data.TablaPrograma>> GetAllAsync()
        {
            return _dal.GetAllAsync();
        }

        public data.TablaPrograma GetOneById(long id)
        {
            return _dal.GetOneById(id);
        }

        public Task<data.TablaPrograma> GetOneByIdAsync(long id)
        {
            return _dal.GetOneByIdAsync(id);
        }

        public void Insert(data.TablaPrograma t)
        {
            _dal.Insert(t);
        }

        public void Update(data.TablaPrograma t)
        {
            _dal.Update(t);
        }
    }
}
