﻿using BE.DAL.DO.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using data = BE.DAL.DO.Objects;
using dal = BE.DAL;
using BE.DAL.FE;

namespace BE.BS
{
    public class Categoria : ICRUD<data.TablaCategoria>
    {
        private dal.Categoria _dal;
        public Categoria(DBContext dbContext)
        {
            _dal = new dal.Categoria(dbContext);
        }
        public void Delete(data.TablaCategoria t)
        {
            _dal.Delete(t);
        }

        public IEnumerable<data.TablaCategoria> GetAll()
        {
            return _dal.GetAll();
        }

        public Task<IEnumerable<data.TablaCategoria>> GetAllAsync()
        {
            return _dal.GetAllAsync();
        }

        public data.TablaCategoria GetOneById(long id)
        {
            return _dal.GetOneById(id);
        }

        public Task<data.TablaCategoria> GetOneByIdAsync(long id)
        {
            return _dal.GetOneByIdAsync(id);
        }

        public void Insert(data.TablaCategoria t)
        {
            _dal.Insert(t);
        }

        public void Update(data.TablaCategoria t)
        {
            _dal.Update(t);
        }
    }
}
