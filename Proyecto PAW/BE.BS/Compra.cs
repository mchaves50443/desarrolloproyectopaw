﻿using System;
using System.Collections.Generic;
using System.Text;
using data = BE.DAL.DO.Objects;
using dal = BE.DAL;
using BE.DAL;
using BE.DAL.FE;
using BE.DAL.DO.Interfaces;
using System.Threading.Tasks;

namespace BE.BS
{
    public class Compra : ICRUD<data.TablaCompra>
    {
        private dal.Compra _dal;
        public Compra(DBContext dbContext)
        {
            _dal = new dal.Compra(dbContext);
        }
        public void Delete(data.TablaCompra t)
        {
            _dal.Delete(t);
        }

        public IEnumerable<data.TablaCompra> GetAll()
        {
            return null;
        }

        public Task<IEnumerable<data.TablaCompra>> GetAllAsync()
        {
            return _dal.GetAllAsync();
        }

        public data.TablaCompra GetOneById(long id)
        {
            return _dal.GetOneById(id);
        }

        public Task<data.TablaCompra> GetOneByIdAsync(long id)
        {
            return _dal.GetOneByIdAsync(id);
        }

        public void Insert(data.TablaCompra t)
        {
            _dal.Insert(t);
        }

        public void Update(data.TablaCompra t)
        {
            _dal.Update(t);
        }
    }
}
