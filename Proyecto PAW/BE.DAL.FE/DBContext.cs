﻿using BE.DAL.DO.Objects;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BE.DAL.FE
{
    public partial class DBContext : DbContext
    {
        public DBContext()
        {
        }

        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TablaActividades> TablaActividades { get; set; }
        public virtual DbSet<TablaCategoria> TablaCategoria { get; set; }
        public virtual DbSet<TablaCompra> TablaCompra { get; set; }
        public virtual DbSet<TablaEjercicios> TablaEjercicios { get; set; }
        public virtual DbSet<TablaLog> TablaLog { get; set; }
        public virtual DbSet<TablaPedido> TablaPedido { get; set; }
        public virtual DbSet<TablaProducto> TablaProducto { get; set; }
        public virtual DbSet<TablaPrograma> TablaPrograma { get; set; }
        public virtual DbSet<TablaProgramaEjercicio> TablaProgramaEjercicio { get; set; }
        public virtual DbSet<TablaProveedor> TablaProveedor { get; set; }
        public virtual DbSet<TablaRutina> TablaRutina { get; set; }
        public virtual DbSet<TablaUsuario> TablaUsuario { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DESKTOP-K5SVPP1;Database=ProyectoPrograAvanzada;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TablaActividades>(entity =>
            {
                entity.HasKey(e => e.IdActividad)
                    .HasName("PK__TablaAct__C5FAF47E04ECCF14");

                entity.Property(e => e.IdActividad).HasColumnName("ID_Actividad");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("descripcion")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasColumnType("datetime");

                entity.Property(e => e.MaxInventario).HasColumnName("maxInventario");

                entity.Property(e => e.MinInventario).HasColumnName("minInventario");

                entity.Property(e => e.NombreActivIdAdes)
                    .IsRequired()
                    .HasColumnName("nombreActivID_ades")
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.Precio)
                    .HasColumnName("precio")
                    .HasColumnType("decimal(10, 0)");

                entity.Property(e => e.TipoActividad).HasColumnName("tipoActividad");
            });

            modelBuilder.Entity<TablaCategoria>(entity =>
            {
                entity.HasKey(e => e.IdCategoria)
                    .HasName("PK__TablaCat__02AA078564E637CE");

                entity.Property(e => e.IdCategoria).HasColumnName("ID_Categoria");

                entity.Property(e => e.NombreCategoria)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TablaCompra>(entity =>
            {
                entity.HasKey(e => e.IdCompra)
                    .HasName("PK__TablaCom__A9D5994E20247405");

                entity.Property(e => e.IdCompra).HasColumnName("ID_Compra");

                entity.Property(e => e.Cantidad).HasColumnName("cantidad");

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdProducto).HasColumnName("ID_Producto");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.Total)
                    .HasColumnName("total")
                    .HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.TablaCompra)
                    .HasForeignKey(d => d.IdProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TablaComp__ID_Pr__45F365D3");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.TablaCompra)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TablaComp__ID_Us__46E78A0C");
            });


            modelBuilder.Entity<TablaEjercicios>(entity =>
            {
                entity.HasKey(e => e.IdEjercicio)
                    .HasName("PK_TablaEjercicio");

                entity.Property(e => e.IdEjercicio).HasColumnName("ID_Ejercicio");

                entity.Property(e => e.LinkVideo)
                    .IsRequired()
                    .HasColumnName("link_video")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NombreEjercicio)
                    .HasColumnName("Nombre_Ejercicio")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });


            modelBuilder.Entity<TablaLog>(entity =>
            {
                entity.HasKey(e => e.IdLog)
                    .HasName("PK__TablaLog__2DBF339555219877");

                entity.Property(e => e.IdLog).HasColumnName("ID_Log");

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.IdActividad).HasColumnName("ID_Actividad");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.HasOne(d => d.IdActividadNavigation)
                    .WithMany(p => p.TablaLog)
                    .HasForeignKey(d => d.IdActividad)
                    .HasConstraintName("FK__TablaLog__ID_Act__4CA06362");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.TablaLog)
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("FK__TablaLog__ID_Usu__4D94879B");
            });

            modelBuilder.Entity<TablaPedido>(entity =>
            {
                entity.HasKey(e => e.IdPedido)
                    .HasName("PK__TablaPed__FD768070F82F3CC9");

                entity.Property(e => e.IdPedido).HasColumnName("ID_Pedido");

                entity.Property(e => e.Cantidad).HasColumnName("cantidad");

                entity.Property(e => e.FechaEntrega)
                    .HasColumnName("fecha_entrega")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaPedido)
                    .HasColumnName("fecha_pedido")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdProducto).HasColumnName("ID_Producto");

                entity.Property(e => e.Total).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.TablaPedido)
                    .HasForeignKey(d => d.IdProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TablaPedi__ID_Pr__4E88ABD4");
            });

            modelBuilder.Entity<TablaProducto>(entity =>
            {
                entity.HasKey(e => e.IdProducto)
                    .HasName("PK__TablaPro__9B4120E2785C2266");

                entity.Property(e => e.IdProducto).HasColumnName("ID_Producto");

                entity.Property(e => e.CantInventario).HasColumnName("cantInventario");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.IdCategoria).HasColumnName("ID_Categoria");

                entity.Property(e => e.IdProveedor).HasColumnName("ID_Proveedor");

                entity.Property(e => e.NombreProducto)
                    .IsRequired()
                    .HasColumnName("nombreProducto")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Precio)
                    .HasColumnName("precio")
                    .HasColumnType("decimal(10, 0)");

                entity.HasOne(d => d.IdCategoriaNavigation)
                    .WithMany(p => p.TablaProducto)
                    .HasForeignKey(d => d.IdCategoria)
                    .HasConstraintName("ID_Categoria");

                entity.HasOne(d => d.IdProveedorNavigation)
                    .WithMany(p => p.TablaProducto)
                    .HasForeignKey(d => d.IdProveedor)
                    .HasConstraintName("FK__TablaProd__ID_Pr__4F7CD00D");
            });

            modelBuilder.Entity<TablaPrograma>(entity =>
            {
                entity.HasKey(e => e.IdPrograma);

                entity.Property(e => e.IdPrograma).HasColumnName("ID_Programa");

                entity.Property(e => e.CantVentas).HasColumnName("cant_ventas");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Precio).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TituloPrograma)
                    .IsRequired()
                    .HasColumnName("Titulo_Programa")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TablaProgramaEjercicio>(entity =>
            {
                entity.HasKey(e => e.IdProgramaEjercicio)
                    .HasName("PK__TablaPro__06124FF619BAB463");

                entity.Property(e => e.IdProgramaEjercicio).HasColumnName("ID_ProgramaEjercicio");

                entity.Property(e => e.FkIdEjercicio).HasColumnName("FK_ID_Ejercicio");

                entity.Property(e => e.FkIdPrograma).HasColumnName("FK_ID_Programa");

                entity.Property(e => e.NReps).HasColumnName("N_Reps");

                entity.Property(e => e.NSets).HasColumnName("N_Sets");

                entity.Property(e => e.Notas)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.FkIdEjercicioNavigation)
                    .WithMany(p => p.TablaProgramaEjercicio)
                    .HasForeignKey(d => d.FkIdEjercicio)
                    .HasConstraintName("FK_Ejercicio");

                entity.HasOne(d => d.FkIdProgramaNavigation)
                    .WithMany(p => p.TablaProgramaEjercicio)
                    .HasForeignKey(d => d.FkIdPrograma)
                    .HasConstraintName("FK_Programa_Ejerc");
            });

            modelBuilder.Entity<TablaProveedor>(entity =>
            {
                entity.HasKey(e => e.IdProveedor)
                    .HasName("PK__TablaPro__7D65272FD95C4D2C");

                entity.Property(e => e.IdProveedor).HasColumnName("ID_Proveedor");

                entity.Property(e => e.ApellidoProveedor)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.NombreProveedor)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TelefonoProveedor)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TablaRutina>(entity =>
            {
                entity.HasKey(e => e.IdRutina)
                    .HasName("PK__TablaRut__7E4618A6C8052CB8");

                entity.Property(e => e.IdRutina).HasColumnName("ID_Rutina");

                entity.Property(e => e.FechaFin)
                    .HasColumnName("fecha_fin")
                    .HasColumnType("datetime");

                entity.Property(e => e.FechaInicio)
                    .HasColumnName("fecha_inicio")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdPrograma).HasColumnName("ID_Programa");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.HasOne(d => d.IdProgramaNavigation)
                    .WithMany(p => p.TablaRutina)
                    .HasForeignKey(d => d.IdPrograma)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TablaRuti__ID_Pr__534D60F1");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.TablaRutina)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TablaRuti__ID_Us__5441852A");
            });

            modelBuilder.Entity<TablaUsuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario);

                entity.HasIndex(e => e.CorreoElectronico)
                    .HasName("UQ__TablaUsu__85947816AEFD114E")
                    .IsUnique();

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.Contrasena)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CorreoElectronico)
                    .IsRequired()
                    .HasColumnName("Correo_Electronico")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreUsuario)
                    .IsRequired()
                    .HasColumnName("Nombre_Usuario")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrimerApellido)
                    .IsRequired()
                    .HasColumnName("Primer_Apellido")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SegundoApellido)
                    .IsRequired()
                    .HasColumnName("Segundo_Apellido")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
