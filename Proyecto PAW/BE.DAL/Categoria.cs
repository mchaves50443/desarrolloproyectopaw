﻿using data = BE.DAL.DO.Objects;
using BE.DAL.DO.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BE.DAL.Repository;
using BE.DAL.FE;
using BE.DAL.DO.Objects;

namespace BE.DAL
{
    public class Categoria : ICRUD<data.TablaCategoria>
    {
        private Repository<data.TablaCategoria> repo;
        public Categoria(DBContext dbContext)
        {
            repo = new Repository<data.TablaCategoria>(dbContext);
        }
        public void Delete(data.TablaCategoria t)
        {
            repo.Delete(t);
            repo.Commit();
        }

        public IEnumerable<data.TablaCategoria> GetAll()
        {
            return repo.GetAll();
        }

        public Task<IEnumerable<data.TablaCategoria>> GetAllAsync()
        {
            return null;
        }

        public data.TablaCategoria GetOneById(long id)
        {
            return repo.GetOnebyID(id);
        }



        public Task<data.TablaCategoria> GetOneByIdAsync(long id)
        {
            return null;
        }



        public void Insert(data.TablaCategoria t)
        {
            repo.Insert(t);
            repo.Commit();
        }

        public void Update(data.TablaCategoria t)
        {
            repo.Update(t);
            repo.Commit();
        }
    }
}
