﻿using data = BE.DAL.DO.Objects;
using BE.DAL.DO.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BE.DAL.Repository;
using BE.DAL.FE;
using BE.DAL.DO.Objects;

namespace BE.DAL
{
    public class Programa : ICRUD<data.TablaPrograma>
    {
        private Repository<data.TablaPrograma> repo;
        public Programa(DBContext dbContext)
        {
            repo = new Repository<data.TablaPrograma>(dbContext);
        }
        public void Delete(data.TablaPrograma t)
        {
            repo.Delete(t);
            repo.Commit();
        }

        public IEnumerable<data.TablaPrograma> GetAll()
        {
            return repo.GetAll();
        }

        public Task<IEnumerable<data.TablaPrograma>> GetAllAsync()
        {
            return null;
        }

        public data.TablaPrograma GetOneById(long id)
        {
            return repo.GetOnebyID(id);
        }

       

        public Task<data.TablaPrograma> GetOneByIdAsync(long id)
        {
            return null;
        }

      

        public void Insert(data.TablaPrograma t)
        {
            repo.Insert(t);
            repo.Commit();
        }

        public void Update(data.TablaPrograma t)
        {
            repo.Update(t);
            repo.Commit();
        }
    }
}
