﻿using System;
using System.Collections.Generic;
using System.Text;
using data = BE.DAL.DO.Objects;
using BE.DAL.DO.Interfaces;
using BE.DAL.Repository;
using BE.DAL.FE;
using System.Threading.Tasks;

namespace BE.DAL
{
    public class Pedido : ICRUD<data.TablaPedido>
    {
        private RepositoryPedido repo;
        public Pedido(DBContext dbContext)
        {
            repo = new RepositoryPedido(dbContext);
        }
        public void Delete(data.TablaPedido t)
        {
            repo.Delete(t);
            repo.Commit();
        }

        public IEnumerable<data.TablaPedido> GetAll()
        {
            return repo.GetAll();
        }

        public Task<IEnumerable<data.TablaPedido>> GetAllAsync()
        {
            return repo.GetAllAsync();
        }

        public data.TablaPedido GetOneById(long id)
        {
            return repo.GetOnebyID(id);
        }


        public Task<data.TablaPedido> GetOneByIdAsync(long PedidoId)
        {
            return repo.GetOneByIdAsync(PedidoId);
        }


        public void Insert(data.TablaPedido t)
        {
            repo.Insert(t);
            repo.Commit();
        }

        public void Update(data.TablaPedido t)
        {
            repo.Update(t);
            repo.Commit();
        }
    }
}
