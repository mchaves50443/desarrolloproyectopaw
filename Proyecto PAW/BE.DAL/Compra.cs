﻿using System;
using System.Collections.Generic;
using System.Text;
using data = BE.DAL.DO.Objects;
using BE.DAL.DO.Interfaces;
using BE.DAL.Repository;
using BE.DAL.FE;
using System.Threading.Tasks;

namespace BE.DAL
{
    public class Compra : ICRUD<data.TablaCompra>
    {
        private RepositoryCompra repo;
        public Compra(DBContext dbContext)
        {
            repo = new RepositoryCompra(dbContext);
        }
        public void Delete(data.TablaCompra t)
        {
            repo.Delete(t);
            repo.Commit();
        }

        public IEnumerable<data.TablaCompra> GetAll()
        {
            return repo.GetAll();
        }

        public Task<IEnumerable<data.TablaCompra>> GetAllAsync()
        {
            return repo.GetAllAsync();
        }

        public data.TablaCompra GetOneById(long id)
        {
            return repo.GetOnebyID(id);
        }


        public Task<data.TablaCompra> GetOneByIdAsync(long CompraId)
        {
            return repo.GetOneByIdAsync(CompraId);
        }


        public void Insert(data.TablaCompra t)
        {
            repo.Insert(t);
            repo.Commit();
        }

        public void Update(data.TablaCompra t)
        {
            repo.Update(t);
            repo.Commit();
        }
    }
}
