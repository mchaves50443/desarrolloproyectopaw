﻿using System;
using System.Collections.Generic;
using System.Text;
using data = BE.DAL.DO.Objects;
using BE.DAL.DO.Interfaces;
using BE.DAL.Repository;
using BE.DAL.FE;
using System.Threading.Tasks;

namespace BE.DAL
{
    public class Producto : ICRUD<data.TablaProducto>
    {
        private RepositoryProducto repo;
        public Producto(DBContext dbContext)
        {
            repo = new RepositoryProducto(dbContext);
        }
        public void Delete(data.TablaProducto t)
        {
            repo.Delete(t);
            repo.Commit();
        }

        public IEnumerable<data.TablaProducto> GetAll()
        {
            return repo.GetAll();
        }

        public Task<IEnumerable<data.TablaProducto>> GetAllAsync()
        {
            return repo.GetAllAsync();
        }

        public data.TablaProducto GetOneById(long id)
        {
            return repo.GetOnebyID(id);
        }


        public Task<data.TablaProducto> GetOneByIdAsync(long ProductoId)
        {
            return repo.GetOneByIdAsync(ProductoId);
        }


        public void Insert(data.TablaProducto t)
        {
            repo.Insert(t);
            repo.Commit();
        }

        public void Update(data.TablaProducto t)
        {
            repo.Update(t);
            repo.Commit();
        }
    }
}
