﻿using data = BE.DAL.DO.Objects;
using BE.DAL.DO.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BE.DAL.Repository;
using BE.DAL.FE;
using BE.DAL.DO.Objects;
namespace BE.DAL
{
    public class Actividades: ICRUD<data.TablaActividades>
    {
        private Repository<data.TablaActividades> repo;
        public Actividades(DBContext dbContext)
        {
            repo = new Repository<data.TablaActividades>(dbContext);
        }
        public void Delete(data.TablaActividades t)
        {
            repo.Delete(t);
            repo.Commit();
        }

        public IEnumerable<data.TablaActividades> GetAll()
        {
            return repo.GetAll();
        }

        public Task<IEnumerable<data.TablaActividades>> GetAllAsync()
        {
            return null;
        }

        public data.TablaActividades GetOneById(long id)
        {
            return repo.GetOnebyID(id);
        }



        public Task<data.TablaActividades> GetOneByIdAsync(long id)
        {
            return null;
        }



        public void Insert(data.TablaActividades t)
        {
            repo.Insert(t);
            repo.Commit();
        }

        public void Update(data.TablaActividades t)
        {
            repo.Update(t);
            repo.Commit();
        }
    }
}
