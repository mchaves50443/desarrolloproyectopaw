﻿using data = BE.DAL.DO.Objects;
using BE.DAL.DO.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BE.DAL.Repository;
using BE.DAL.FE;
using BE.DAL.DO.Objects;

namespace BE.DAL
{
    public class Ejercicios : ICRUD<data.TablaEjercicios>
    {
        private Repository<data.TablaEjercicios> repo;
        public Ejercicios(DBContext dbContext)
        {
            repo = new Repository<data.TablaEjercicios>(dbContext);
        }
        public void Delete(data.TablaEjercicios t)
        {
            repo.Delete(t);
            repo.Commit();
        }

        public IEnumerable<data.TablaEjercicios> GetAll()
        {
            return repo.GetAll();
        }

        public Task<IEnumerable<data.TablaEjercicios>> GetAllAsync()
        {
            return null;
        }

        public data.TablaEjercicios GetOneById(long id)
        {
            return repo.GetOnebyID(id);
        }



        public Task<data.TablaEjercicios> GetOneByIdAsync(long id)
        {
            return null;
        }



        public void Insert(data.TablaEjercicios t)
        {
            repo.Insert(t);
            repo.Commit();
        }

        public void Update(data.TablaEjercicios t)
        {
            repo.Update(t);
            repo.Commit();
        }
    }
}
