﻿using System;
using System.Collections.Generic;
using System.Text;
using data = BE.DAL.DO.Objects;
using BE.DAL.DO.Interfaces;
using BE.DAL.Repository;
using BE.DAL.FE;
using System.Threading.Tasks;

namespace BE.DAL
{
    public class Rutina : ICRUD<data.TablaRutina>
    {
        private RepositoryRutina repo;
        public Rutina(DBContext dbContext)
        {
            repo = new RepositoryRutina(dbContext);
        }
        public void Delete(data.TablaRutina t)
        {
            repo.Delete(t);
            repo.Commit();
        }

        public IEnumerable<data.TablaRutina> GetAll()
        {
            return repo.GetAll();
        }

        public Task<IEnumerable<data.TablaRutina>> GetAllAsync()
        {
            return repo.GetAllAsync();
        }

        public data.TablaRutina GetOneById(long id)
        {
            return repo.GetOnebyID(id);
        }


        public Task<data.TablaRutina> GetOneByIdAsync(long CompraId)
        {
            return repo.GetOneByIdAsync(CompraId);
        }


        public void Insert(data.TablaRutina t)
        {
            repo.Insert(t);
            repo.Commit();
        }

        public void Update(data.TablaRutina t)
        {
            repo.Update(t);
            repo.Commit();
        }
    }
}
