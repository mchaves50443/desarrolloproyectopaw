﻿using data = BE.DAL.DO.Objects;
using BE.DAL.DO.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BE.DAL.Repository;
using BE.DAL.FE;
using BE.DAL.DO.Objects;


namespace BE.DAL
{
    public class Usuario : ICRUD<data.TablaUsuario>
    {
        private Repository<data.TablaUsuario> repo;
        public Usuario(DBContext dbContext)
        {
            repo = new Repository<data.TablaUsuario>(dbContext);
        }
        public void Delete(data.TablaUsuario t)
        {
            repo.Delete(t);
            repo.Commit();
        }

        public IEnumerable<data.TablaUsuario> GetAll()
        {
            return repo.GetAll();
        }

        public Task<IEnumerable<data.TablaUsuario>> GetAllAsync()
        {
            return null;
        }

        public data.TablaUsuario GetOneById(long id)
        {
            return repo.GetOnebyID(id);
        }


        public data.TablaUsuario GetLogin(string user, string password)
        {
            return repo.GetLogin(user,password);
        }



        public Task<data.TablaUsuario> GetOneByIdAsync(long id)
        {
            return null;
        }



        public void Insert(data.TablaUsuario t)
        {
            repo.Insert(t);
            repo.Commit();
        }

        public void Update(data.TablaUsuario t)
        {
            repo.Update(t);
            repo.Commit();
        }
    }
}
