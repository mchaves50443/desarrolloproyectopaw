﻿using data = BE.DAL.DO.Objects;
using BE.DAL.DO.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BE.DAL.Repository;
using BE.DAL.FE;
using BE.DAL.DO.Objects;

namespace BE.DAL
{
   public class Proveedor : ICRUD<data.TablaProveedor>
    {
        private Repository<data.TablaProveedor> repo;
        public Proveedor(DBContext dbContext)
        {
            repo = new Repository<data.TablaProveedor>(dbContext);
        }
        public void Delete(data.TablaProveedor t)
        {
            repo.Delete(t);
            repo.Commit();
        }

        public IEnumerable<data.TablaProveedor> GetAll()
        {
            return repo.GetAll();
        }

        public Task<IEnumerable<data.TablaProveedor>> GetAllAsync()
        {
            return null;
        }

        public data.TablaProveedor GetOneById(long id)
        {
            return repo.GetOnebyID(id);
        }



        public Task<data.TablaProveedor> GetOneByIdAsync(long id)
        {
            return null;
        }



        public void Insert(data.TablaProveedor t)
        {
            repo.Insert(t);
            repo.Commit();
        }

        public void Update(data.TablaProveedor t)
        {
            repo.Update(t);
            repo.Commit();
        }
    }
}
