﻿using System;
using System.Collections.Generic;
using System.Text;
using data = BE.DAL.DO.Objects;
using BE.DAL.DO.Interfaces;
using BE.DAL.Repository;
using BE.DAL.FE;
using System.Threading.Tasks;

namespace BE.DAL
{
    public class Log : ICRUD<data.TablaLog>
    {
        private RepositoryLog repo;
        public Log(DBContext dbContext)
        {
            repo = new RepositoryLog(dbContext);
        }
        public void Delete(data.TablaLog t)
        {
            repo.Delete(t);
            repo.Commit();
        }

        public IEnumerable<data.TablaLog> GetAll()
        {
            return repo.GetAll();
        }

        public Task<IEnumerable<data.TablaLog>> GetAllAsync()
        {
            return repo.GetAllAsync();
        }

        public data.TablaLog GetOneById(long id)
        {
            return repo.GetOnebyID(id);
        }


        public Task<data.TablaLog> GetOneByIdAsync(long LogId)
        {
            return repo.GetOneByIdAsync(LogId);
        }


        public void Insert(data.TablaLog t)
        {
            repo.Insert(t);
            repo.Commit();
        }

        public void Update(data.TablaLog t)
        {
            repo.Update(t);
            repo.Commit();
        }
    }
}
