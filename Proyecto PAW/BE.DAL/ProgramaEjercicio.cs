﻿using System;
using System.Collections.Generic;
using System.Text;
using data = BE.DAL.DO.Objects;
using BE.DAL.DO.Interfaces;
using BE.DAL.Repository;
using BE.DAL.FE;
using System.Threading.Tasks;
using BE.DAL.DO.Objects;

namespace BE.DAL
{

    public class ProgramaEjercicio : ICRUD<data.TablaProgramaEjercicio>
    {
        private RepositoryProgramaEjercicio repo;
        public ProgramaEjercicio(DBContext dbContext)
        {
            repo = new RepositoryProgramaEjercicio(dbContext);
        }
        public void Delete(data.TablaProgramaEjercicio t)
        {
            repo.DeleteAsync(t);
            repo.Commit();
        }

        public IEnumerable<data.TablaProgramaEjercicio> GetAll()
        {
            return repo.GetAll();
        }

        public Task<IEnumerable<data.TablaProgramaEjercicio>> GetAllAsync()
        {
            return repo.GetAllAsync();
        }

        public data.TablaProgramaEjercicio GetOneById(long id)
        {
            return null;
        }


        public Task<data.TablaProgramaEjercicio> GetOneByIdAsync(long programaId)
        {
            return repo.GetOneByIdAsync(programaId);
        }

        public void Insert(data.TablaProgramaEjercicio t)
        {
            repo.Insert(t);
            repo.Commit();
        }

        public void Update(data.TablaProgramaEjercicio t)
        {
            repo.UpdateAsync(t);
            repo.Commit();
        }

        
    }
}
