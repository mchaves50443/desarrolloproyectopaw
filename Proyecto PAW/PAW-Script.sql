USE [master]
GO
/****** Object:  Database [ProyectoPrograAvanzada]    Script Date: 12/7/2021 12:05:07 PM ******/
CREATE DATABASE [ProyectoPrograAvanzada]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ProyectoPrograAvanzada', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\ProyectoPrograAvanzada.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ProyectoPrograAvanzada_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\ProyectoPrograAvanzada_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ProyectoPrograAvanzada].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET ARITHABORT OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET  MULTI_USER 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET QUERY_STORE = OFF
GO
USE [ProyectoPrograAvanzada]
GO
/****** Object:  Table [dbo].[TablaActividades]    Script Date: 12/7/2021 12:05:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TablaActividades](
	[ID_Actividad] [bigint] IDENTITY(1,1) NOT NULL,
	[nombreActivID_ades] [varchar](225) NOT NULL,
	[precio] [decimal](10, 0) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[descripcion] [varchar](255) NOT NULL,
	[tipoActividad] [smallint] NOT NULL,
	[minInventario] [smallint] NOT NULL,
	[maxInventario] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Actividad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TablaCategoria]    Script Date: 12/7/2021 12:05:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TablaCategoria](
	[ID_Categoria] [bigint] IDENTITY(1,1) NOT NULL,
	[NombreCategoria] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Categoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TablaDetalleFactura]    Script Date: 12/7/2021 12:05:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TablaDetalleFactura](
	[ID_DetalleFactura] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Factura] [bigint] NULL,
	[ID_Programa] [bigint] NULL,
	[ID_Producto] [bigint] NULL,
	[ID_Workshop] [bigint] NULL,
	[ID_Actividad] [bigint] NULL,
	[cantidad] [int] NULL,
	[precio] [decimal](10, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_DetalleFactura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TablaEjercicios]    Script Date: 12/7/2021 12:05:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TablaEjercicios](
	[ID_Ejercicio] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre_Ejercicio] [varchar](30) NULL,
	[link_video] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TablaEjercicio] PRIMARY KEY CLUSTERED 
(
	[ID_Ejercicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TablaFactura]    Script Date: 12/7/2021 12:05:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TablaFactura](
	[ID_Factura] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_ID_Usuario] [bigint] NOT NULL,
	[Total] [decimal](18, 2) NOT NULL,
	[Fecha] [date] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Apellidos] [varchar](150) NOT NULL,
	[Correo_electronico] [varchar](150) NOT NULL,
	[Telefono] [varchar](30) NULL,
	[Pais] [varchar](50) NULL,
	[Provincia] [varchar](50) NULL,
	[Canton] [varchar](50) NULL,
	[Distrito] [varchar](50) NULL,
	[Direccion] [varchar](150) NULL,
	[Codigo_postal] [varchar](10) NULL,
 CONSTRAINT [PK_TablaFactura] PRIMARY KEY CLUSTERED 
(
	[ID_Factura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TablaImagen]    Script Date: 12/7/2021 12:05:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TablaImagen](
	[ID_Imagen] [bigint] IDENTITY(1,1) NOT NULL,
	[nombreImagen] [varchar](100) NOT NULL,
	[datosImagen] [varbinary](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Imagen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TablaLog]    Script Date: 12/7/2021 12:05:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TablaLog](
	[ID_Log] [bigint] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](300) NOT NULL,
	[Fecha] [date] NOT NULL,
	[Modulo] [varchar](45) NOT NULL,
	[Accion] [varchar](45) NOT NULL,
	[Usuario] [varchar](45) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Log] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TablaProducto]    Script Date: 12/7/2021 12:05:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TablaProducto](
	[ID_Producto] [bigint] IDENTITY(1,1) NOT NULL,
	[nombreProducto] [varchar](50) NOT NULL,
	[descripcion] [varchar](150) NULL,
	[cantInventario] [int] NOT NULL,
	[precio] [decimal](10, 0) NOT NULL,
	[ID_Imagen] [bigint] NULL,
	[ID_Categoria] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Producto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TablaPrograma]    Script Date: 12/7/2021 12:05:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TablaPrograma](
	[ID_Programa] [bigint] IDENTITY(1,1) NOT NULL,
	[Titulo_Programa] [varchar](50) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
	[cant_ventas] [bigint] NULL,
 CONSTRAINT [PK_TablaPrograma] PRIMARY KEY CLUSTERED 
(
	[ID_Programa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TablaProgramaEjercicio]    Script Date: 12/7/2021 12:05:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TablaProgramaEjercicio](
	[ID_ProgramaEjercicio] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_ID_Programa] [bigint] NOT NULL,
	[FK_ID_Ejercicio] [bigint] NOT NULL,
	[N_Sets] [smallint] NOT NULL,
	[N_Reps] [smallint] NOT NULL,
	[Notas] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_ProgramaEjercicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TablaUsuario]    Script Date: 12/7/2021 12:05:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TablaUsuario](
	[ID_Usuario] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre_Usuario] [varchar](50) NOT NULL,
	[Primer_Apellido] [varchar](50) NOT NULL,
	[Segundo_Apellido] [varchar](50) NOT NULL,
	[Correo_Electronico] [varchar](50) NOT NULL,
	[Contrasena] [varchar](50) NOT NULL,
	[Rol] [smallint] NOT NULL,
 CONSTRAINT [PK_TablaUsuario] PRIMARY KEY CLUSTERED 
(
	[ID_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Correo_Electronico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TablaWorkshop]    Script Date: 12/7/2021 12:05:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TablaWorkshop](
	[ID_Workshop] [bigint] IDENTITY(1,1) NOT NULL,
	[nombreWorkshop] [varchar](255) NOT NULL,
	[minInventario] [int] NOT NULL,
	[maxInventatio] [int] NOT NULL,
	[precio] [decimal](10, 0) NOT NULL,
	[fecha] [date] NOT NULL,
	[descripcion] [varchar](300) NOT NULL,
	[ID_Imagen] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Workshop] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TablaDetalleFactura] ADD  DEFAULT (NULL) FOR [ID_Factura]
GO
ALTER TABLE [dbo].[TablaDetalleFactura] ADD  DEFAULT (NULL) FOR [ID_Programa]
GO
ALTER TABLE [dbo].[TablaDetalleFactura] ADD  DEFAULT (NULL) FOR [ID_Producto]
GO
ALTER TABLE [dbo].[TablaDetalleFactura] ADD  DEFAULT (NULL) FOR [ID_Workshop]
GO
ALTER TABLE [dbo].[TablaDetalleFactura] ADD  DEFAULT (NULL) FOR [ID_Actividad]
GO
ALTER TABLE [dbo].[TablaDetalleFactura] ADD  DEFAULT (NULL) FOR [cantidad]
GO
ALTER TABLE [dbo].[TablaDetalleFactura] ADD  DEFAULT (NULL) FOR [precio]
GO
ALTER TABLE [dbo].[TablaProducto] ADD  DEFAULT (NULL) FOR [descripcion]
GO
ALTER TABLE [dbo].[TablaDetalleFactura]  WITH CHECK ADD  CONSTRAINT [ID_Actividad] FOREIGN KEY([ID_Actividad])
REFERENCES [dbo].[TablaActividades] ([ID_Actividad])
GO
ALTER TABLE [dbo].[TablaDetalleFactura] CHECK CONSTRAINT [ID_Actividad]
GO
ALTER TABLE [dbo].[TablaDetalleFactura]  WITH CHECK ADD  CONSTRAINT [ID_Factura] FOREIGN KEY([ID_Factura])
REFERENCES [dbo].[TablaFactura] ([ID_Factura])
GO
ALTER TABLE [dbo].[TablaDetalleFactura] CHECK CONSTRAINT [ID_Factura]
GO
ALTER TABLE [dbo].[TablaDetalleFactura]  WITH CHECK ADD  CONSTRAINT [ID_Producto] FOREIGN KEY([ID_Producto])
REFERENCES [dbo].[TablaProducto] ([ID_Producto])
GO
ALTER TABLE [dbo].[TablaDetalleFactura] CHECK CONSTRAINT [ID_Producto]
GO
ALTER TABLE [dbo].[TablaDetalleFactura]  WITH CHECK ADD  CONSTRAINT [ID_Programa] FOREIGN KEY([ID_Programa])
REFERENCES [dbo].[TablaPrograma] ([ID_Programa])
GO
ALTER TABLE [dbo].[TablaDetalleFactura] CHECK CONSTRAINT [ID_Programa]
GO
ALTER TABLE [dbo].[TablaDetalleFactura]  WITH CHECK ADD  CONSTRAINT [ID_Workshop] FOREIGN KEY([ID_Workshop])
REFERENCES [dbo].[TablaWorkshop] ([ID_Workshop])
GO
ALTER TABLE [dbo].[TablaDetalleFactura] CHECK CONSTRAINT [ID_Workshop]
GO
ALTER TABLE [dbo].[TablaFactura]  WITH CHECK ADD  CONSTRAINT [FK_ID_Usuario] FOREIGN KEY([FK_ID_Usuario])
REFERENCES [dbo].[TablaUsuario] ([ID_Usuario])
GO
ALTER TABLE [dbo].[TablaFactura] CHECK CONSTRAINT [FK_ID_Usuario]
GO
ALTER TABLE [dbo].[TablaProducto]  WITH CHECK ADD  CONSTRAINT [ID_Categoria] FOREIGN KEY([ID_Categoria])
REFERENCES [dbo].[TablaCategoria] ([ID_Categoria])
GO
ALTER TABLE [dbo].[TablaProducto] CHECK CONSTRAINT [ID_Categoria]
GO
ALTER TABLE [dbo].[TablaProducto]  WITH CHECK ADD  CONSTRAINT [ID_ImagenPD] FOREIGN KEY([ID_Imagen])
REFERENCES [dbo].[TablaImagen] ([ID_Imagen])
GO
ALTER TABLE [dbo].[TablaProducto] CHECK CONSTRAINT [ID_ImagenPD]
GO
ALTER TABLE [dbo].[TablaProgramaEjercicio]  WITH CHECK ADD  CONSTRAINT [FK_Ejercicio] FOREIGN KEY([FK_ID_Ejercicio])
REFERENCES [dbo].[TablaEjercicios] ([ID_Ejercicio])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TablaProgramaEjercicio] CHECK CONSTRAINT [FK_Ejercicio]
GO
ALTER TABLE [dbo].[TablaProgramaEjercicio]  WITH CHECK ADD  CONSTRAINT [FK_Programa_Ejerc] FOREIGN KEY([FK_ID_Programa])
REFERENCES [dbo].[TablaPrograma] ([ID_Programa])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TablaProgramaEjercicio] CHECK CONSTRAINT [FK_Programa_Ejerc]
GO
ALTER TABLE [dbo].[TablaWorkshop]  WITH CHECK ADD  CONSTRAINT [ID_ImagenWK] FOREIGN KEY([ID_Imagen])
REFERENCES [dbo].[TablaImagen] ([ID_Imagen])
GO
ALTER TABLE [dbo].[TablaWorkshop] CHECK CONSTRAINT [ID_ImagenWK]
GO
USE [master]
GO
ALTER DATABASE [ProyectoPrograAvanzada] SET  READ_WRITE 
GO
