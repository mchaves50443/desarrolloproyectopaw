﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BE.DAL.DO.Objects
{
    public class TablaProveedor
    {
        public TablaProveedor()
        {
            TablaProducto = new HashSet<TablaProducto>();
        }

        public long IdProveedor { get; set; }
        public string NombreProveedor { get; set; }
        public string ApellidoProveedor { get; set; }
        public string TelefonoProveedor { get; set; }

        public virtual ICollection<TablaProducto> TablaProducto { get; set; }
    }
}
