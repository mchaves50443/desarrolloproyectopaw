﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BE.DAL.DO.Objects
{
    public class TablaEjercicios
    {
        public TablaEjercicios()
        {
            TablaProgramaEjercicio = new HashSet<TablaProgramaEjercicio>();
        }

        public long IdEjercicio { get; set; }
        public string NombreEjercicio { get; set; }
        public string LinkVideo { get; set; }

        public virtual ICollection<TablaProgramaEjercicio> TablaProgramaEjercicio { get; set; }
    }
}
