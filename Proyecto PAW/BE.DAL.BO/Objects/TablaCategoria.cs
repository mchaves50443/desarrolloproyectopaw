﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BE.DAL.DO.Objects
{
    public class TablaCategoria
    {
        public TablaCategoria()
        {
            TablaProducto = new HashSet<TablaProducto>();
        }

        public long IdCategoria { get; set; }
        public string NombreCategoria { get; set; }

        public virtual ICollection<TablaProducto> TablaProducto { get; set; }
    }
}
