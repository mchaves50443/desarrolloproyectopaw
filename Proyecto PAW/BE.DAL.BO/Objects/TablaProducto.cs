﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BE.DAL.DO.Objects
{
    public class TablaProducto
    {
        public TablaProducto()
        {
           
            TablaPedido = new HashSet<TablaPedido>();
            TablaCompra = new HashSet<TablaCompra>();
        }

        public long IdProducto { get; set; }
        public string NombreProducto { get; set; }
        public string Descripcion { get; set; }
        public int CantInventario { get; set; }
        public decimal Precio { get; set; }
        public long? IdCategoria { get; set; }
        public long? IdProveedor { get; set; }

        public virtual TablaCategoria IdCategoriaNavigation { get; set; }
        public virtual TablaProveedor IdProveedorNavigation { get; set; }
        public virtual ICollection<TablaPedido> TablaPedido { get; set; }
        public virtual ICollection<TablaCompra> TablaCompra { get; set; }
    }
}
