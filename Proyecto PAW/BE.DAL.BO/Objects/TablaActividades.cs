﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BE.DAL.DO.Objects
{
    public class TablaActividades
    {
        public TablaActividades()
        {
            TablaLog = new HashSet<TablaLog>();
        }

        public long IdActividad { get; set; }
        public string NombreActivIdAdes { get; set; }
        public decimal Precio { get; set; }
        public DateTime Fecha { get; set; }
        public string Descripcion { get; set; }
        public short TipoActividad { get; set; }
        public short MinInventario { get; set; }
        public short MaxInventario { get; set; }

        public virtual ICollection<TablaLog> TablaLog { get; set; }
    }
}
