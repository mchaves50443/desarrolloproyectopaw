﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BE.DAL.DO.Objects
{
    public class TablaLog
    {
        public long IdLog { get; set; }
        public long? IdUsuario { get; set; }
        public long? IdActividad { get; set; }
        public DateTime? Fecha { get; set; }

        public virtual TablaActividades IdActividadNavigation { get; set; }
        public virtual TablaUsuario IdUsuarioNavigation { get; set; }
    }
}
