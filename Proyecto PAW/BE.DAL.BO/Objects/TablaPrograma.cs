﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BE.DAL.DO.Objects
{
    public class TablaPrograma
    {
        public TablaPrograma()
        {
            TablaProgramaEjercicio = new HashSet<TablaProgramaEjercicio>();
        }

        public long IdPrograma { get; set; }
        public string TituloPrograma { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
        public long? CantVentas { get; set; }

        public virtual ICollection<TablaProgramaEjercicio> TablaProgramaEjercicio { get; set; }
        public virtual ICollection<TablaRutina> TablaRutina { get; set; }
    }
}
