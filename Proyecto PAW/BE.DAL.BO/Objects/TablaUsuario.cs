﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BE.DAL.DO.Objects
{
    public class TablaUsuario
    {
        public TablaUsuario()
        {
            TablaLog = new HashSet<TablaLog>();
            TablaCompra = new HashSet<TablaCompra>();
        }

        public long IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string CorreoElectronico { get; set; }
        public string Contrasena { get; set; }
        public short Rol { get; set; }

        public virtual ICollection<TablaLog> TablaLog { get; set; }
        public virtual ICollection<TablaCompra> TablaCompra { get; set; }
        public virtual ICollection<TablaRutina> TablaRutina { get; set; }
    }
}
