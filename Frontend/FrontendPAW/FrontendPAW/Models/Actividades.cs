﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontendPAW.Models
{
    public class Actividades
    {
        public long IdActividad { get; set; }
        public string NombreActivIdAdes { get; set; }
        public decimal Precio { get; set; }
        public DateTime Fecha { get; set; }
        public string Descripcion { get; set; }
        public short TipoActividad { get; set; }
        public short MinInventario { get; set; }
        public short MaxInventario { get; set; }
    }
}
