﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontendPAW.Models
{
    public class Categoria
    {
        public long IdCategoria { get; set; }
        public string NombreCategoria { get; set; }
    }
}
