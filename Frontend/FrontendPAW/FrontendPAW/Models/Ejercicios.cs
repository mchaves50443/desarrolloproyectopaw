﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontendPAW.Models
{
    public class Ejercicios
    {
        public long IdEjercicio { get; set; }
        public string NombreEjercicio { get; set; }
        public string LinkVideo { get; set; }
    }
}
