﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontendPAW.Models
{
    public class Pedido
    {
        public long IdPedido { get; set; }
        public long IdProducto { get; set; }
        public int Cantidad { get; set; }
        public DateTime FechaPedido { get; set; }
        public DateTime FechaEntrega { get; set; }

        public virtual Producto IdProductoNavigation { get; set; }
    }
}
