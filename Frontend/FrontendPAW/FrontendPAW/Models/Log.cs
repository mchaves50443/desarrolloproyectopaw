﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontendPAW.Models
{
    public class Log
    {
        public long IdLog { get; set; }
        public long? IdUsuario { get; set; }
        public long? IdActividad { get; set; }
        public DateTime? Fecha { get; set; }

        public virtual Actividades IdActividadNavigation { get; set; }
        public virtual Usuarios IdUsuarioNavigation { get; set; }
    }
}
