﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontendPAW.Models
{
    public class ProgramaEjercicio
    {
        public long IdProgramaEjercicio { get; set; }
        public long FkIdPrograma { get; set; }
        public long FkIdEjercicio { get; set; }
        public short NSets { get; set; }
        public short NReps { get; set; }
        public string Notas { get; set; }

        public virtual Ejercicios FkIdEjercicioNavigation { get; set; }
        public virtual Programas FkIdProgramaNavigation { get; set; }
    }
}
