﻿using FrontendPAW.Models;
using FrontendPAW.Servicios;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FrontendPAW.Controllers
{
    public class LogController : Controller
    {
        ActividadServices actser = new ActividadServices();
        UsuarioServices userser = new UsuarioServices();
        
        public LogController()
        {
           
        }

        // GET: Log
        public async Task<IActionResult> Index()
        {
            List<Models.Log> aux = new List<Models.Log>();
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await cl.GetAsync("api/Log");

                if (res.IsSuccessStatusCode)
                {
                    var auxres = res.Content.ReadAsStringAsync().Result;
                    aux = JsonConvert.DeserializeObject<List<Models.Log>>(auxres);
                }
            }
            return View(aux);
        }

        // GET: TablaLog/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aux = GetById(id);
            if (aux == null)
            {
                return NotFound();
            }

            return View(aux);
        }

        // GET: TablaLog/Create
        public IActionResult Create()
        {

            ViewData["IdActividad"] = new SelectList(actser.GetAll(), "IdActividad", "NombreActivIdAdes");
            ViewData["IdUsuario"] = new SelectList(userser.GetAll(), "IdUsuario", "CorreoElectronico");
            return View();
        }

        // POST: TablaLog/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdLog,IdActividad, IdUsuario,Fecha")] Log tablaLog)
        {
            if (ModelState.IsValid)
            {
                using (var cl = new HttpClient())
                {
                    cl.BaseAddress = new Uri(Program.baseurl);
                    var content = JsonConvert.SerializeObject(tablaLog);
                    var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                    var postTask = cl.PostAsync("api/Log", byteContent).Result;

                    if (postTask.IsSuccessStatusCode)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                }
            }
            ViewData["IdActividad"] = new SelectList(actser.GetAll(), "IdActividad", "NombreActivIdAdes", tablaLog.IdActividad);
            ViewData["IdUsuario"] = new SelectList(userser.GetAll(), "IdUsuario", "CorreoElectronico", tablaLog.IdUsuario);
            return View(tablaLog);
        }

        // GET: TablaLog/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aux = GetById(id);
            if (aux == null)
            {
                return NotFound();
            }
            ViewData["IdActividad"] = new SelectList(actser.GetAll(), "IdActividad", "NombreActivIdAdes", aux.IdActividad);
            ViewData["IdUsuario"] = new SelectList(userser.GetAll(), "IdUsuario", "CorreoElectronico", aux.IdUsuario);

            return View(aux);
        }

        // POST: TablaLog/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long IdLog, [Bind("IdLog,IdActividad,IdUsuario,Fecha")] Log tablaLog)
        {
            if (IdLog != tablaLog.IdLog)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    using (var cl = new HttpClient())
                    {
                        cl.BaseAddress = new Uri(Program.baseurl);
                        var content = JsonConvert.SerializeObject(tablaLog);
                        var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                        var byteContent = new ByteArrayContent(buffer);
                        byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                        var postTask = cl.PutAsync("api/Log/" + IdLog, byteContent).Result;

                        if (postTask.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception)
                {
                    var aux2 = GetById(IdLog);
                    if (aux2 == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["IdActividad"] = new SelectList(actser.GetAll(), "IdActividad", "NombreActivIdAdes", tablaLog.IdActividad);
            ViewData["IdUsuario"] = new SelectList(userser.GetAll(), "IdUsuario", "CorreoElectronico", tablaLog.IdUsuario);
            return View(tablaLog);
           
       
        }

        // GET: TablaLog/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aux = GetById(id);
            if (aux == null)
            {
                return NotFound();
            }

            return View(aux);
        }

        // POST: TablaLog/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long IdLog)
        {
            var categoria = GetById(IdLog);
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await cl.DeleteAsync("api/Log/" + IdLog);

                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction(nameof(Index));
        }

        public Models.Log GetById(long? id)
        {
            Models.Log aux = new Models.Log();
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //HttpResponseMessage res = await cl.GetAsync("api/Pais/5?"+id);
                HttpResponseMessage res = cl.GetAsync("api/Log/" + id).Result;

                if (res.IsSuccessStatusCode)
                {
                    var auxres = res.Content.ReadAsStringAsync().Result;
                    aux = JsonConvert.DeserializeObject<Models.Log>(auxres);
                }
            }
            return aux;
        }
    }
}
