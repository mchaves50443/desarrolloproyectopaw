﻿using FrontendPAW.Models;
using FrontendPAW.Servicios;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FrontendPAW.Controllers
{
    public class RutinaController : Controller
    {
        ProgramasServices progser = new ProgramasServices();
        UsuarioServices userser = new UsuarioServices();

        public RutinaController()
        {
        
        }

        // GET: Rutina
        public async Task<IActionResult> Index()
        {
            List<Models.Rutina> aux = new List<Models.Rutina>();
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await cl.GetAsync("api/Rutina");

                if (res.IsSuccessStatusCode)
                {
                    var auxres = res.Content.ReadAsStringAsync().Result;
                    aux = JsonConvert.DeserializeObject<List<Models.Rutina>>(auxres);
                }
            }
            return View(aux);
        }

        // GET: Rutina/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aux = GetById(id);
            if (aux == null)
            {
                return NotFound();
            }

            return View(aux);
        }

        // GET: Rutina/Create
        public IActionResult Create()
        {
            ViewData["IdPrograma"] = new SelectList(progser.GetAll(), "IdPrograma", "TituloPrograma");
            ViewData["IdUsuario"] = new SelectList(userser.GetAll(), "IdUsuario", "CorreoElectronico");
            return View();
        }

        // POST: Rutina/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdRutina,IdUsuario,IdPrograma,FechaInicio,FechaFin")] Rutina rutina)
        {
            if (ModelState.IsValid)
            {
                using (var cl = new HttpClient())
                {
                    cl.BaseAddress = new Uri(Program.baseurl);
                    var content = JsonConvert.SerializeObject(rutina);
                    var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                    var postTask = cl.PostAsync("api/Rutina", byteContent).Result;

                    if (postTask.IsSuccessStatusCode)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                }
            }
            ViewData["IdPrograma"] = new SelectList(progser.GetAll(), "IdPrograma", "TituloPrograma", rutina.IdPrograma);
            ViewData["IdUsuario"] = new SelectList(userser.GetAll(), "IdUsuario", "CorreoElectronico", rutina.IdUsuario);
            return View(rutina);
        }

        // GET: Rutina/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aux = GetById(id);
            if (aux == null)
            {
                return NotFound();
            }
            ViewData["IdPrograma"] = new SelectList(progser.GetAll(), "IdPrograma", "TituloPrograma", aux.IdPrograma);
            ViewData["IdUsuario"] = new SelectList(userser.GetAll(), "IdUsuario", "CorreoElectronico", aux.IdUsuario);

            return View(aux);
        }

        // POST: Rutina/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long IdRutina, [Bind("IdRutina,IdUsuario,IdPrograma,FechaInicio,FechaFin")] Rutina rutina)
        {
            if (IdRutina != rutina.IdRutina)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    using (var cl = new HttpClient())
                    {
                        cl.BaseAddress = new Uri(Program.baseurl);
                        var content = JsonConvert.SerializeObject(rutina);
                        var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                        var byteContent = new ByteArrayContent(buffer);
                        byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                        var postTask = cl.PutAsync("api/Rutina/" + IdRutina, byteContent).Result;

                        if (postTask.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception)
                {
                    var aux2 = GetById(IdRutina);
                    if (aux2 == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["IdPrograma"] = new SelectList(progser.GetAll(), "IdPrograma", "TituloPrograma", rutina.IdPrograma);
            ViewData["IdUsuario"] = new SelectList(userser.GetAll(), "IdUsuario", "CorreoElectronico", rutina.IdUsuario);
            return View(rutina);
        }

        // GET: Rutina/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aux = GetById(id);
            if (aux == null)
            {
                return NotFound();
            }

            return View(aux);
        }

        // POST: Rutina/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long IdRutina)
        {
            var categoria = GetById(IdRutina);
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await cl.DeleteAsync("api/Rutina/" + IdRutina);

                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction(nameof(Index));
        }

        public Models.Rutina GetById(long? id)
        {
            Models.Rutina aux = new Models.Rutina();
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //HttpResponseMessage res = await cl.GetAsync("api/Pais/5?"+id);
                HttpResponseMessage res = cl.GetAsync("api/Rutina/" + id).Result;

                if (res.IsSuccessStatusCode)
                {
                    var auxres = res.Content.ReadAsStringAsync().Result;
                    aux = JsonConvert.DeserializeObject<Models.Rutina>(auxres);
                }
            }
            return aux;
        }
    }
}

