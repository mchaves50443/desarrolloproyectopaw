﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FrontendPAW.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string user, string password)
        {
            if (user == null || user == "" || password == null || password == "")
            {
                return RedirectToAction("SignIn");
            }


            var aux = GetLogin(user,password);

            if (aux.IdUsuario>0 && aux.NombreUsuario!=null && aux.Contrasena!= null)
            {
                return RedirectToAction("Index","Home");
            }
            else
            {
                return RedirectToAction("SignIn");
            }
        }


        public Models.Usuarios GetLogin(string user, string password)
        {
            Models.Usuarios aux = new Models.Usuarios();
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //HttpResponseMessage res = await cl.GetAsync("api/Pais/5?"+id);
                HttpResponseMessage res = cl.GetAsync("api/Usuario/login/" + user+"/"+password).Result;

                if (res.IsSuccessStatusCode)
                {
                    var auxres = res.Content.ReadAsStringAsync().Result;
                    aux = JsonConvert.DeserializeObject<Models.Usuarios>(auxres);
                }
            }
            return aux;
        }
    }
}
