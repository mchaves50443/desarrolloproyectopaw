﻿using FrontendPAW.Models;
using FrontendPAW.Servicios;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FrontendPAW.Controllers
{
    public class CompraController : Controller
    {
        UsuarioServices user = new UsuarioServices();
        ProductoServices pro = new ProductoServices();

        public CompraController()
        {
          
        }

        // GET: Compra
        public async Task<IActionResult> Index()
        {
            List<Models.Compra> aux = new List<Models.Compra>();
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await cl.GetAsync("api/Compra");

                if (res.IsSuccessStatusCode)
                {
                    var auxres = res.Content.ReadAsStringAsync().Result;
                    aux = JsonConvert.DeserializeObject<List<Models.Compra>>(auxres);
                }
            }
            return View(aux);
        }

        // GET: Compra/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aux = GetById(id);
            if (aux == null)
            {
                return NotFound();
            }

            return View(aux);
        }

        // GET: TablaCompra/Create
        public IActionResult Create()
        {
            ViewData["IdProducto"] = new SelectList(pro.GetAll(), "IdProducto", "NombreProducto");
            ViewData["IdUsuario"] = new SelectList(user.GetAll(), "IdUsuario", "CorreoElectronico");
            return View();
        }

        // POST: TablaCompra/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdCompra,IdUsuario,IdProducto,Cantidad,Total,Fecha")] Compra tablaCompra)
        {
            if (ModelState.IsValid)
            {
                using (var cl = new HttpClient())
                {
                    cl.BaseAddress = new Uri(Program.baseurl);
                    var content = JsonConvert.SerializeObject(tablaCompra);
                    var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                    var postTask = cl.PostAsync("api/Compra", byteContent).Result;

                    if (postTask.IsSuccessStatusCode)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                }
            }
            ViewData["IdProducto"] = new SelectList(pro.GetAll(), "IdProducto", "NombreProducto", tablaCompra.IdProducto);
            ViewData["IdUsuario"] = new SelectList(user.GetAll(), "IdUsuario", "CorreoElectronico", tablaCompra.IdUsuario);
            return View(tablaCompra);
        }

        // GET: TablaCompra/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aux = GetById(id);
            if (aux == null)
            {
                return NotFound();
            }
            ViewData["IdProducto"] = new SelectList(pro.GetAll(), "IdProducto", "NombreProducto", aux.IdProducto);
            ViewData["IdUsuario"] = new SelectList(user.GetAll(), "IdUsuario", "CorreoElectronico", aux.IdUsuario);
            return View(aux);
        }

        // POST: TablaCompra/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long IdCompra, [Bind("IdCompra,IdUsuario,IdProducto,Cantidad,Total,Fecha")] Compra tablaCompra)
        {
            if (IdCompra != tablaCompra.IdCompra)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    using (var cl = new HttpClient())
                    {
                        cl.BaseAddress = new Uri(Program.baseurl);
                        var content = JsonConvert.SerializeObject(tablaCompra);
                        var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                        var byteContent = new ByteArrayContent(buffer);
                        byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                        var postTask = cl.PutAsync("api/Compra/" + IdCompra, byteContent).Result;

                        if (postTask.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception)
                {
                    var aux2 = GetById(IdCompra);
                    if (aux2 == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdProducto"] = new SelectList(pro.GetAll(), "IdProducto", "NombreProducto", tablaCompra.IdProducto);
            ViewData["IdUsuario"] = new SelectList(user.GetAll(), "IdUsuario", "CorreoElectronico", tablaCompra.IdUsuario);
            return View(tablaCompra);
        }

        // GET: TablaCompra/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aux = GetById(id);
            if (aux == null)
            {
                return NotFound();
            }

            return View(aux);
        }

        // POST: TablaCompra/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long IdCompra)
        {
            var Compra = GetById(IdCompra);
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await cl.DeleteAsync("api/Compra/" + IdCompra);

                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction(nameof(Index));
        }

        public Models.Compra GetById(long? id)
        {
            Models.Compra aux = new Models.Compra();
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //HttpResponseMessage res = await cl.GetAsync("api/Pais/5?"+id);
                HttpResponseMessage res = cl.GetAsync("api/Compra/" + id).Result;

                if (res.IsSuccessStatusCode)
                {
                    var auxres = res.Content.ReadAsStringAsync().Result;
                    aux = JsonConvert.DeserializeObject<Models.Compra>(auxres);
                }
            }
            return aux;
        }
    }
}
