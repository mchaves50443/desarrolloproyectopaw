﻿using FrontendPAW.Models;
using FrontendPAW.Servicios;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FrontendPAW.Controllers
{
    public class ProgramaEjercicioController : Controller
    {
        ProgramasServices serProg = new ProgramasServices();
        EjerciciosServices serEjer = new EjerciciosServices();
        public ProgramaEjercicioController()
        {
            
        }

        // GET: ProgramaEjercicio
        public async Task<IActionResult> Index()
        {
            List<Models.ProgramaEjercicio> aux = new List<Models.ProgramaEjercicio>();
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await cl.GetAsync("api/ProgramaEjercicio");

                if (res.IsSuccessStatusCode)
                {
                    var auxres = res.Content.ReadAsStringAsync().Result;
                    aux = JsonConvert.DeserializeObject<List<Models.ProgramaEjercicio>>(auxres);
                }
            }
            return View(aux);
        }

        // GET: ProgramaEjercicio/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var foci = GetById(id);
            if (foci == null)
            {
                return NotFound();
            }

            return View(foci);
        }

        // GET: ProgramaEjercicio/Create
        public IActionResult Create()
        {
            ViewData["FkIdEjercicio"] = new SelectList(serEjer.GetAll(), "IdEjercicio", "NombreEjercicio");
            ViewData["FkIdPrograma"] = new SelectList(serProg.GetAll(), "IdPrograma", "TituloPrograma");
            return View();
        }

        // POST: ProgramaEjercicios/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdProgramaEjercicio,FkIdPrograma,FkIdEjercicio,NSets,NReps,Notas")] ProgramaEjercicio tablaProgramaEjercicio)
        {
            if (ModelState.IsValid)
            {
                using (var cl = new HttpClient())
                {
                    cl.BaseAddress = new Uri(Program.baseurl);
                    var content = JsonConvert.SerializeObject(tablaProgramaEjercicio);
                    var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                    var postTask = cl.PostAsync("api/ProgramaEjercicio", byteContent).Result;

                    if (postTask.IsSuccessStatusCode)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                }
            }
            ViewData["FkIdEjercicio"] = new SelectList(serEjer.GetAll(), "IdEjercicio", "NombreEjercicio", tablaProgramaEjercicio.FkIdEjercicio);
            ViewData["FkIdPrograma"] = new SelectList(serProg.GetAll(), "IdPrograma", "TituloPrograma", tablaProgramaEjercicio.FkIdPrograma);
            return View(tablaProgramaEjercicio);
        }

        // GET: ProgramaEjercicios/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tablaProgramaEjercicio = GetById(id);
            if (tablaProgramaEjercicio == null)
            {
                return NotFound();
            }
            ViewData["FkIdEjercicio"] = new SelectList(serEjer.GetAll(), "IdEjercicio", "NombreEjercicio", tablaProgramaEjercicio.FkIdEjercicio);
            ViewData["FkIdPrograma"] = new SelectList(serProg.GetAll(), "IdPrograma", "TituloPrograma", tablaProgramaEjercicio.FkIdPrograma);
            return View(tablaProgramaEjercicio);
        }

        // POST: ProgramaEjercicios/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long IdProgramaEjercicio, [Bind("IdProgramaEjercicio,FkIdPrograma,FkIdEjercicio,NSets,NReps,Notas")] ProgramaEjercicio tablaProgramaEjercicio)
        {
            if (IdProgramaEjercicio != tablaProgramaEjercicio.IdProgramaEjercicio)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    using (var cl = new HttpClient())
                    {
                        cl.BaseAddress = new Uri(Program.baseurl);
                        var content = JsonConvert.SerializeObject(tablaProgramaEjercicio);
                        var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                        var byteContent = new ByteArrayContent(buffer);
                        byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                        var postTask = cl.PutAsync("api/ProgramaEjercicio/" + IdProgramaEjercicio, byteContent).Result;

                        if (postTask.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception)
                {
                    var aux2 = GetById(IdProgramaEjercicio);
                    if (aux2 == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["FkIdEjercicio"] = new SelectList(serEjer.GetAll(), "IdEjercicio", "NombreEjercicio", tablaProgramaEjercicio.FkIdEjercicio);
            ViewData["FkIdPrograma"] = new SelectList(serProg.GetAll(), "IdPrograma", "TituloPrograma", tablaProgramaEjercicio.FkIdPrograma);
            return View(tablaProgramaEjercicio);
        }

        // GET: ProgramaEjercicios/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tablaProgramaEjercicio = GetById(id);
            if (tablaProgramaEjercicio == null)
            {
                return NotFound();
            }

            return View(tablaProgramaEjercicio);
        }

        // POST: ProgramaEjercicios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long IdProgramaEjercicio)
        {
            var tablaProgramaEjercicio = GetById(IdProgramaEjercicio);
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await cl.DeleteAsync("api/ProgramaEjercicio/" + IdProgramaEjercicio);

                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction(nameof(Index));
        }

        public Models.ProgramaEjercicio GetById(long? id)
        {
            Models.ProgramaEjercicio aux = new Models.ProgramaEjercicio();
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //HttpResponseMessage res = await cl.GetAsync("api/Pais/5?"+id);
                HttpResponseMessage res = cl.GetAsync("api/ProgramaEjercicio/" + id).Result;

                if (res.IsSuccessStatusCode)
                {
                    var auxres = res.Content.ReadAsStringAsync().Result;
                    aux = JsonConvert.DeserializeObject<Models.ProgramaEjercicio>(auxres);
                }
            }
            return aux;
        }
    }
}

