﻿using FrontendPAW.Models;
using FrontendPAW.Servicios;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FrontendPAW.Controllers
{
    public class ProductoController : Controller
    {
        CategoriaServices serCat = new CategoriaServices();
        ProveedorServices serImg = new ProveedorServices();

        // GET: Producto
        public async Task<IActionResult> Index()
        {
            List<Models.Producto> aux = new List<Models.Producto>();
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await cl.GetAsync("api/Producto");

                if (res.IsSuccessStatusCode)
                {
                    var auxres = res.Content.ReadAsStringAsync().Result;
                    aux = JsonConvert.DeserializeObject<List<Models.Producto>>(auxres);
                }
            }
            return View(aux);
        }

        // GET: Producto/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aux = GetById(id);
            if (aux == null)
            {
                return NotFound();
            }

            return View(aux);
        }

        // GET: Producto/Create
        public IActionResult Create()
        {
            ViewData["IdCategoria"] = new SelectList(serCat.GetAll(), "IdCategoria", "NombreCategoria");
            ViewData["IdProveedor"] = new SelectList(serImg.GetAll(), "IdProveedor", "NombreProveedor");
            return View();
        }

        // POST: Producto/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdProducto,NombreProducto,Descripcion,CantInventario,Precio,IdProveedor,IdCategoria")] Producto tablaProducto)
        {
            if (ModelState.IsValid)
            {
                using (var cl = new HttpClient())
                {
                    cl.BaseAddress = new Uri(Program.baseurl);
                    var content = JsonConvert.SerializeObject(tablaProducto);
                    var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                    var postTask = cl.PostAsync("api/Producto", byteContent).Result;

                    if (postTask.IsSuccessStatusCode)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                }
            }
            
            ViewData["IdCategoria"] = new SelectList(serCat.GetAll(), "IdCategoria", "NombreCategoria", tablaProducto.IdCategoria);
            ViewData["IdProveedor"] = new SelectList(serImg.GetAll(), "IdProveedor", "NombreProveedor", tablaProducto.IdProveedor);
            return View(tablaProducto);
        }

        // GET: Producto/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aux = GetById(id);
            if (aux == null)
            {
                return NotFound();
            }

            
            ViewData["IdCategoria"] = new SelectList(serCat.GetAll(), "IdCategoria", "NombreCategoria", aux.IdCategoria);
            ViewData["IdProveedor"] = new SelectList(serImg.GetAll(), "IdProveedor", "NombreProveedor", aux.IdProveedor);
            return View(aux);
        }

        // POST: Producto/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long IdProducto, [Bind("IdProducto,NombreProducto,Descripcion,CantInventario,Precio,IdProveedor,IdCategoria")] Producto tablaProducto)
        {
            if (IdProducto != tablaProducto.IdProducto)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    using (var cl = new HttpClient())
                    {
                        cl.BaseAddress = new Uri(Program.baseurl);
                        var content = JsonConvert.SerializeObject(tablaProducto);
                        var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                        var byteContent = new ByteArrayContent(buffer);
                        byteContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                        var postTask = cl.PutAsync("api/Producto/" + IdProducto, byteContent).Result;

                        if (postTask.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception)
                {
                    var aux2 = GetById(IdProducto);
                    if (aux2 == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCategoria"] = new SelectList(serCat.GetAll(), "IdCategoria", "NombreCategoria", tablaProducto.IdCategoria);
            ViewData["IdProveedor"] = new SelectList(serImg.GetAll(), "IdProveedor", "NombreProveedor", tablaProducto.IdProveedor);
            return View(tablaProducto);
        }

        // GET: Producto/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aux = GetById(id);
            if (aux == null)
            {
                return NotFound();
            }

            return View(aux);
        }

        // POST: Producto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long IdProducto)
        {
            var categoria = GetById(IdProducto);
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage res = await cl.DeleteAsync("api/Producto/" + IdProducto);

                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction(nameof(Index));
        }

        public Models.Producto GetById(long? id)
        {
            Models.Producto aux = new Models.Producto();
            using (var cl = new HttpClient())
            {
                cl.BaseAddress = new Uri(Program.baseurl);
                cl.DefaultRequestHeaders.Clear();
                cl.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //HttpResponseMessage res = await cl.GetAsync("api/Pais/5?"+id);
                HttpResponseMessage res = cl.GetAsync("api/Producto/" + id).Result;

                if (res.IsSuccessStatusCode)
                {
                    var auxres = res.Content.ReadAsStringAsync().Result;
                    aux = JsonConvert.DeserializeObject<Models.Producto>(auxres);
                }
            }
            return aux;
        }
    }
}

